package top.ezttf.peoplespringboot.exceptions;

/**
 * @author yuwen
 * @date 2019/1/3
 */
public class PeopleOperationException extends RuntimeException {

    public PeopleOperationException(String message) {
        super(message);
    }
}
