package top.ezttf.peoplespringboot.exceptions;

/**
 * @author yuwen
 * @date 2019/1/4
 */
public class DomicileOperationException extends RuntimeException {

    public DomicileOperationException(String msg) {
        super(msg);
    }
}
