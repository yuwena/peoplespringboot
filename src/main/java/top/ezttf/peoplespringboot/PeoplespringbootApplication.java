package top.ezttf.peoplespringboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
@MapperScan(basePackages = {"top.ezttf.peoplespringboot.dao"})
public class PeoplespringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(PeoplespringbootApplication.class, args);
    }

}

