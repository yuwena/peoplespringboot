package top.ezttf.peoplespringboot.interceptors.dao;

import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.TypeHandlerRegistry;
import top.ezttf.peoplespringboot.annotations.ResultPair;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@Slf4j
@Intercepts(value = @Signature(
        method = "handleResultSets",
        type = ResultSetHandler.class,
        args = {Statement.class}
))
public class ResultPairInterceptor implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MetaObject metaObject = SystemMetaObject.forObject(invocation.getTarget());
        // metaObject 是代理对象 类型为 Proxy, 其中有 h属性, 获取 h属性中是ibatis 中的 Plugin类, 其中有target属性
        while (metaObject.hasGetter("h")) {
            metaObject = SystemMetaObject.forObject(metaObject.getValue("h"));
        }
        while (metaObject.hasGetter("target")) {
            metaObject = SystemMetaObject.forObject(metaObject.getValue("target"));
        }
        MappedStatement mappedStatement = (MappedStatement) metaObject.getValue("mappedStatement");
        Class clazz = Class.forName(StringUtils.substringBeforeLast(mappedStatement.getId(), "."));
        Method method = findMethod(clazz, StringUtils.substringAfterLast(mappedStatement.getId(), "."));
        if (method == null || !method.isAnnotationPresent(ResultPair.class)) {
            return invocation.proceed();
        }
        ResultPair annotation = method.getAnnotation(ResultPair.class);
        Statement statement = (Statement) invocation.getArgs()[0];
        Pair<Class<?>, Class<?>> kvType = getKVType(method);
        TypeHandlerRegistry registry = mappedStatement.getConfiguration().getTypeHandlerRegistry();
        return wrapResult(statement, registry, kvType, annotation);
    }


    @Override
    public Object plugin(Object target) {
        // 返回为当前 target创建的动态代理
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }


    /**
     * 查找类中方法名, 由于同一namespace下必没有同名方法, 无需指定方法参数类型
     *
     * @param clazz      类
     * @param methodName 方法名
     * @return 方法
     */
    private Method findMethod(Class clazz, String methodName) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (StringUtils.equals(methodName, method.getName())) {
                return method;
            }
        }
        return null;
    }


    /**
     * 泛型类型获取
     *
     * @param method 方法
     * @return 泛型类型对
     */
    private Pair<Class<?>, Class<?>> getKVType(Method method) {
        Type returnType = method.getGenericReturnType();
        if (returnType instanceof ParameterizedType) {
            if (((ParameterizedType) returnType).getRawType().getClass().isAssignableFrom(Map.class)) {
                throw new RuntimeException("the method returned by the @ResultPair annotation must instance of java.util.Map");
            }
            Type[] typeArguments = ((ParameterizedType) returnType).getActualTypeArguments();
            return new Pair<>(typeArguments[0].getClass(), typeArguments[1].getClass());
        }
        return new Pair<>(null, null);
    }


    /**
     * 重新包装返回值 , 将其作为key-value返回
     *
     * @param statement
     * @param registry
     * @param pair
     * @param resultPair
     * @return
     * @throws SQLException
     */
    private Object wrapResult(
            Statement statement,
            TypeHandlerRegistry registry,
            Pair<Class<?>, Class<?>> pair,
            ResultPair resultPair
    ) throws SQLException {

        ResultSet resultSet = statement.getResultSet();
        List<Object> list = new ArrayList<>();
        Map<Object, Object> map = new HashMap<>(1 << 4);
        while (resultSet.next()) {
            Object key = this.getObject(resultSet, 1, registry, pair.getKey());
            Object value = this.getObject(resultSet, 2, registry, pair.getValue());
            if (map.containsKey(key) && !resultPair.allowOverrideValue()) {
                throw new RuntimeException("the same key is not allowed while allowOverride value set to false");
            }
            map.put(key, value);
        }
        list.add(map);
        return list;
    }

    /**
     * 获取返回值
     *
     * @param resultSet
     * @param columnIndex
     * @param typeHandlerRegistry
     * @param javaType
     * @return
     * @throws SQLException
     */
    private Object getObject(
            ResultSet resultSet,
            int columnIndex,
            TypeHandlerRegistry typeHandlerRegistry,
            Class<?> javaType
    ) throws SQLException {
        final TypeHandler<?> typeHandler = typeHandlerRegistry.hasTypeHandler(javaType)
                ? typeHandlerRegistry.getTypeHandler(javaType)
                : typeHandlerRegistry.getUnknownTypeHandler();
        return typeHandler.getResult(resultSet, columnIndex);

    }
}
