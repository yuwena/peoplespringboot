package top.ezttf.peoplespringboot.interceptors.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import top.ezttf.peoplespringboot.common.Const;
import top.ezttf.peoplespringboot.common.ResponseCode;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.pojo.Upload;
import top.ezttf.peoplespringboot.util.JsonUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 登陆校验拦截器
 *
 * @author yuwen
 * @date 2018/12/29
 */
@Slf4j
public class UploadLoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        HttpSession session = request.getSession();
        Upload upload = (Upload) session.getAttribute(Const.CURRENT_USER);
        if (upload == null) {
            response.setContentType("application/json;charset=UTF-8");
            try (PrintWriter out = response.getWriter()){
                out.write(JsonUtil.obj2Str(
                        ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登陆")
                ));
                out.flush();
            }
            return false;
        }
        return true;
    }
}
