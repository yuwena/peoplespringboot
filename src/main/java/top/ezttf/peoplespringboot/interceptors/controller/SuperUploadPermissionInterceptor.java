package top.ezttf.peoplespringboot.interceptors.controller;

import org.springframework.web.servlet.HandlerInterceptor;
import top.ezttf.peoplespringboot.common.Const;
import top.ezttf.peoplespringboot.common.ResponseCode;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.pojo.Upload;
import top.ezttf.peoplespringboot.util.JsonUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 超管权限拦截器
 *
 * @author yuwen
 * @date 2018/12/29
 */
public class SuperUploadPermissionInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        HttpSession session = request.getSession();
        Upload upload = (Upload) session.getAttribute(Const.CURRENT_USER);
        if (upload.getRole() != 1) {
            response.setContentType("application/json;charset=UTF-8");
            try (PrintWriter out = response.getWriter()){
                out.write(JsonUtil
                   .obj2Str(ServerResponse
                   .createByErrorCodeMessage(ResponseCode.PERMISSION_DENIED.getCode(), "无权限操作")));
                out.flush();
            }
            return false;
        }
        return true;
    }
}
