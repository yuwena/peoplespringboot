package top.ezttf.peoplespringboot.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author yuwen
 * @date 2018/12/27
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Domicile {
    private Integer domicileId;

    private String domicileCode;

    private Integer uploadId;


    private String animal;

    private Integer carCount;

    private String carCategory;

    private Integer internet;

    private String others;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;




}