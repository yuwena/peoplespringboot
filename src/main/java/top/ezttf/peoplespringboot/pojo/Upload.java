package top.ezttf.peoplespringboot.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author yuwen
 * @date 2018/12/27
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Upload {

    private Integer uploadId;
    private String username;
    private String password;
    private String uploadArea;
    private Integer role;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @JsonIgnore
    public boolean isSuperAdmin() {
        return this.role == 1;
    }
}