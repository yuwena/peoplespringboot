package top.ezttf.peoplespringboot.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author yuwen
 * @date 2018/12/27
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class People {

    private Integer peopleId;
    private Integer uploadId;

    private String cardId;

    private String domicileCode;

    private String peopleName;

    private Integer sex;

    private String nowSpace;

    private String phone;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    private String relation;

    private String peopleNational;

    private String residence;

    private String domicileLocation;

    private Integer isWidows;

    private String religion;

    private String culture;

    private String political;

    private String organization;

    private String peopleWork;

    private Double earning;

    private String armyCategory;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate inTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate outTime;

    private String forces;

    private String place;

    private Integer isFlowPeople;

    private String marriage;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate marryTime;

    private String birthCard;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate oneChildTime;

    private String oneChildCard;
    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate healthTime;

    private String blood;

    private String healthHistory;

    private String homeHistory;

    private String oldHistory;

    private String medicine;

    private Integer isFuShe;

    private Integer isCommunityRemedy;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate remedyTime;

    private Integer isRelease;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate helpTime;

    private Integer isFaLun;

    private Integer isTurn;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate turnTime;

    private String cultCategory;

    private Integer isDrug;

    private String houseProper;

    private Integer houseCount;

    private Double houseSize;

    private String houseOwner;

    private String useProper;

    private Double houseTime;

    private Integer isHouseProtect;

    private String houseCategory;

    private Integer isGood;

    private Integer isJob;

    private String jobName;

    private String reason;

    private String reJob;

    private String jobIntention;

    private Integer isSecurity;

    private String securityCategory;

    private String joinSecurityCategory;

    private String pensionCategory;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}