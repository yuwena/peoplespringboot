package top.ezttf.peoplespringboot.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.formula.functions.Column;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import top.ezttf.peoplespringboot.exceptions.PeopleOperationException;
import top.ezttf.peoplespringboot.pojo.Domicile;
import top.ezttf.peoplespringboot.pojo.People;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author yuwen
 * @date 2019/1/4
 */
public class ExcelUtil {

    // private static final String EXCEL_PATH = "/home/developer/files/excel/";
    private static final String EXCEL_PATH = "E:/developer/files/excel/";
    private static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();

    public static String verifyFileName(String fileName) {
        if (StringUtils.isNotBlank(fileName)) {
            String suffix = StringUtils.substringAfterLast(fileName, ".");
            if (StringUtils.equals(suffix, "xls") || StringUtils.equals(suffix, "xlsx")) {
                return suffix;
            }
        }
        throw new RuntimeException("excel格式不合法");
    }


    public static Domicile excel2Domicile(MultipartFile file) throws IOException {
        String suffix = StringUtils.substringAfterLast(file.getOriginalFilename(), ".");
        File excel = new File("domicile." + suffix);
        file.transferTo(excel);
        return null;
    }


    private static LocalDate checkDateTypeAndReturn(String dateStr, int row, int column) throws Exception {
        LocalDate result = null;
        try {
            result = DateUtil.str2Date(dateStr);
        } catch (Exception e) {
            throw new PeopleOperationException("excel中第 "
                    + (row + 1)
                    + " 行, 第 "
                    + (column + 1)
                    + " 列中日期格式错误, 正确格式如: 1999-11-11");
        }
        return result;
    }

    private static Integer checkIntTypeAndReturn(String intStr, int row, int column) throws Exception {
        Integer result = null;
        try {
            result = Integer.valueOf(intStr);
        } catch (NumberFormatException e) {
            throw new RuntimeException("excel中第 "
                    + (row + 1)
                    + " 行, 第 "
                    + (column + 1)
                    + " 列中必须为整数");
        }
        return result;
    }

    private static Double checkDoubleTypeAndReturn(String intStr, int row, int column) throws Exception {
        Double result = null;
        try {
            result = Double.valueOf(intStr);
        } catch (NumberFormatException e) {
            throw new RuntimeException("excel中第 "
                    + (row + 1)
                    + " 行, 第 "
                    + (column + 1)
                    + " 列中必须为小数");
        }
        return result;
    }


    private static Integer checkBooleanAndReturnInt(String str, int row, int column) throws Exception {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        if (StringUtils.equals("是", str.trim()) || StringUtils.equals("男", str.trim())) {
            return 1;
        }
        if (StringUtils.equals("否", str.trim()) || StringUtils.equals("女", str.trim())) {
            return 0;
        }
        throw new RuntimeException("检查excel中第 "
                + (row + 1)
                + "行, 第 "
                + (column + 1)
                + " 列信息, 若此列表示性别则只支持填写 \"男\" 或者 \"女\", 若此列不表示性别则只支持填写 \"是\" 或者 \"否\" ");
    }


    private static String checkNullAndReturn(String str, int row, int column) throws Exception {
        if (StringUtils.isBlank(str)) {
            throw new RuntimeException("excel中第 "
                    + (row + 1)
                    + " 行, 第 "
                    + (column + 1)
                    + " 列不能为空值");
        }
        return str;
    }


    /**
     * domicile_id, domicile_code(String), upload_id, security_category(Integer),
     * animal(String), car_count(Integer), car_category(String), create_time, update_time, internet(Integer)
     * , others(String)
     */

    public static List<Domicile> analysisDomicileExcel(MultipartFile file) throws Exception {
        String suffix = ExcelUtil.verifyFileName(file.getOriginalFilename());
        Workbook workbook;
        Sheet sheet;
        Cell cell;
        Row row;
        int columnCount = 0, rowCount = 0;
        try (InputStream inputStream = file.getInputStream()) {
            if (StringUtils.equals(suffix, "xls")) {
                workbook = new HSSFWorkbook(inputStream);
            } else {
                workbook = new XSSFWorkbook(inputStream);
                //workbook = new SXSSFWorkbook((XSSFWorkbook) workbook, 2000);
            }
            sheet = workbook.getSheetAt(0);
            row = sheet.getRow(0);
            // 物理列数, 最大索引会比此值小1
            columnCount = row.getLastCellNum();
            // 物理行数, getLastRowNum 返回的比实际物理行数少 1, 最大索引会比此值小1
            rowCount = sheet.getLastRowNum() + 1;

            if (columnCount != 6) {
                throw new RuntimeException("户籍 excel 表头只可以有 7项信息");
            }
            //else {
            //    if (!StringUtils.equals("户籍码", getCellValue(row.getCell(0)).trim())) {
            //        throw new RuntimeException("户籍excel第一列表头必须为 \"户籍码\"");
            //    }
            //    if (!StringUtils.equals("养殖情况", getCellValue(row.getCell(1)).trim())) {
            //        throw new RuntimeException("户籍excel第一列表头必须为 \"养殖情况\"");
            //    }
            //    if (!StringUtils.equals("汽车数量", getCellValue(row.getCell(2)).trim())) {
            //        throw new RuntimeException("户籍excel第一列表头必须为 \"汽车数量\"");
            //    }
            //    if (!StringUtils.equals("汽车类型", getCellValue(row.getCell(3)).trim())) {
            //        throw new RuntimeException("户籍excel第一列表头必须为 \"汽车类型\"");
            //    }
            //    if (!StringUtils.equals("互联网接入情况", getCellValue(row.getCell(4)).trim())) {
            //        throw new RuntimeException("户籍excel第一列表头必须为 \"互联网接入情况\"");
            //    }
            //    if (!StringUtils.equals("其他信息", getCellValue(row.getCell(5)).trim())) {
            //        throw new RuntimeException("户籍excel第一列表头必须为 \"其他信息\"");
            //    }
            //}

            String cellValue;

            List<Domicile> domicileList = new ArrayList<>();
            // 遍历行, 由于0索引为表头, 所以从1 开始
            for (int i = 1; i < rowCount; i++) {
                Domicile domicile = new Domicile();
                // 遍历列
                for (int j = 0; j < columnCount; j++) {
                    if (sheet.getRow(i) == null) {
                        break;
                    }
                    cell = sheet.getRow(i).getCell(j);
                    cellValue = getCellValue(cell);
                    switch (j) {
                        case 0:
                            String domicileCode = checkNullAndReturn(cellValue, i, j);
                            domicile.setDomicileCode(domicileCode);
                            break;
                        case 1:
                            if (StringUtils.isNotBlank(cellValue)) {
                                domicile.setAnimal(cellValue);
                            }
                            break;
                        case 2:
                            if (StringUtils.isNotBlank(cellValue)) {
                                Integer carCount = checkIntTypeAndReturn(cellValue, i, j);
                                domicile.setCarCount(carCount);
                            }
                            break;
                        case 3:
                            if (StringUtils.isNotBlank(cellValue)) {
                                domicile.setCarCategory(cellValue);
                            }
                            break;
                        case 4:
                            if (StringUtils.isNotBlank(cellValue)) {
                                Integer internet = checkBooleanAndReturnInt(cellValue, i, j);
                                domicile.setInternet(internet);
                            }
                            break;
                        case 5:
                            if (StringUtils.isNotBlank(cellValue)) {
                                domicile.setOthers(cellValue);
                            }
                            break;
                        default:
                    }
                }
                domicileList.add(domicile);
            }
            return domicileList;
        }
    }


    /**
     * people_id, upload_id, card_id, domicile_code, people_name,
     * sex, now_space, phone, birthday, relation,
     * people_national, residence, domicile_location, isWidows, religion,
     * culture, political, organization, people_work, earning,
     * army_category, in_time, out_time, forces, place,
     * is_flow_people, marriage, marry_time, birth_card, one_child_time,
     * one_child_card, address, health_time, blood, health_history,
     * home_history, old_history, medicine, is_fu_she, is_community_remedy,
     * remedy_time, is_release, help_time, is_fa_lun, is_turn,
     * turn_time, cult_category, is_drug, house_proper, house_count,
     * house_size, house_owner, use_proper, house_time, is_house_protect,
     * house_category, is_good, is_job, job_name, reason,
     * re_job, job_intention, is_security, security_category, join_security_category, pension_category, create_time,
     * update_time
     */

    public static List<People> analysisPeopleExcel(MultipartFile file) throws Exception {
        String suffix = ExcelUtil.verifyFileName(file.getOriginalFilename());
        Workbook workbook = null;
        Sheet sheet = null;
        Cell cell = null;
        Row row = null;
        Column column = null;
        int columnCount = 0, rowCount = 0;
        try (InputStream inputStream = file.getInputStream()) {
            if (StringUtils.equals(suffix, "xls")) {
                workbook = new HSSFWorkbook(inputStream);
            } else {
                workbook = new XSSFWorkbook(inputStream);
                //workbook = new SXSSFWorkbook((XSSFWorkbook) workbook, 2000);
            }
        }
        sheet = workbook.getSheetAt(0);
        row = sheet.getRow(0);
        // 物理列数, 最大索引会比此值小1
        columnCount = row.getLastCellNum();
        // 物理行数, getLastRowNum 返回的比实际物理行数少 1, 最大索引会比此值小1
        rowCount = sheet.getLastRowNum() + 1;
        if (columnCount != 64) {
            throw new RuntimeException("人口信息excel必须存在64列信息");
        }
        String cellValue;

        List<People> peopleList = new ArrayList<>();
        for (int i = 1; i < rowCount; i++) {
            People people = new People();
            for (int j = 0; j < columnCount; j++) {
                if (sheet.getRow(i) == null) {
                    break;
                }
                cellValue = getCellValue(sheet.getRow(i).getCell(j));
                switch (j) {
                    // 姓名
                    case 0:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setPeopleName(cellValue);
                        break;
                    // 性别
                    case 1:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        Integer sex = checkBooleanAndReturnInt(cellValue, i, j);
                        people.setSex(sex);
                        break;
                    // 身份证号
                    case 2:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setCardId(cellValue);
                        break;
                    // 出生日期
                    case 3:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        LocalDate birthday = checkDateTypeAndReturn(cellValue, i, j);
                        people.setBirthday(birthday);
                        break;
                    // 户籍码
                    case 4:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setDomicileCode(cellValue);
                        break;
                    // 现居住地
                    case 5:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setNowSpace(cellValue);
                        break;
                    // 联系电话
                    case 6:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setPhone(cellValue);
                        }
                        break;
                    // 与户主关系
                    case 7:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setRelation(cellValue);
                        break;
                    // 民族
                    case 8:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setPeopleNational(cellValue);
                        break;
                    // 户口性质
                    case 9:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setResidence(cellValue);
                        break;
                    // 户籍关系所在地
                    case 10:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setDomicileLocation(cellValue);
                        break;
                    // 是否孤寡
                    case 11:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isWidows = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsWidows(isWidows);
                        }
                        break;
                    //  宗教
                    case 12:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setReligion(cellValue);
                        }
                        break;
                    //   文化程度
                    case 13:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setCulture(cellValue);
                        break;
                    // 政治面貌
                    case 14:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setPolitical(cellValue);
                        break;
                    // 组织关系所在地
                    case 15:
                        cellValue = checkNullAndReturn(cellValue, i, j);
                        people.setOrganization(cellValue);
                        break;
                    // 工作单位
                    case 16:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setPeopleWork(cellValue);
                        }
                        break;
                    // 收入
                    case 17:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Double earning = checkDoubleTypeAndReturn(cellValue, i, j);
                            people.setEarning(earning);
                        }
                        break;
                    //  涉军情况
                    case 18:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setArmyCategory(cellValue);
                        }
                        break;
                    //  入伍时间
                    case 19:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate inTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setInTime(inTime);
                        }
                        break;
                    //  退伍时间
                    case 20:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate outTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setOutTime(outTime);
                        }
                        break;
                    //   兵役部队
                    case 21:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setForces(cellValue);
                        }
                        break;
                    //  安置单位
                    case 22:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setPlace(cellValue);
                        }
                        break;
                    //  是否流动人口
                    case 23:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isFlowPeople = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsFlowPeople(isFlowPeople);
                        }
                        break;
                    //   婚姻状况
                    case 24:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setMarriage(cellValue);
                        }
                        break;
                    //  结婚时间
                    case 25:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate marryTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setMarryTime(marryTime);
                        }
                        break;
                    // 生育证号
                    case 26:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setBirthCard(cellValue);
                        }
                        break;
                    //  独生子女证领取时间
                    case 27:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate oneChildTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setOneChildTime(oneChildTime);
                        }
                        break;
                    //  独生子女证号
                    case 28:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setOneChildCard(cellValue);
                        }
                        break;
                    //  流出入地址
                    case 29:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setAddress(cellValue);
                        }
                        break;
                    //  流出入时间
                    case 30:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate healthTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setHealthTime(healthTime);
                        }
                        //   病史
                    case 31:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setHealthHistory(cellValue);
                        }
                        break;
                    //    家族史
                    case 32:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setHomeHistory(cellValue);
                        }
                        break;
                    //   既往史
                    case 33:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setOldHistory(cellValue);
                        }
                        break;
                    // 药物过敏史
                    case 34:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setMedicine(cellValue);
                        }
                        break;
                    // 是否接触辐射
                    case 35:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isFuShe = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsFuShe(isFuShe);
                        }
                        break;
                    // 血型
                    case 36:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setBlood(cellValue);
                        }
                        break;
                    //  是否社区矫正
                    case 37:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isCommunityRemedy = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsCommunityRemedy(isCommunityRemedy);
                        }
                        break;
                    //  矫正时间
                    case 38:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate remedyTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setRemedyTime(remedyTime);
                        }
                        break;
                    //  是否刑释解教
                    case 39:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isRelease = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsRelease(isRelease);
                        }
                        break;
                    //  帮教时间
                    case 40:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate helpTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setHelpTime(helpTime);
                        }
                        break;
                    // 是否法轮功练习者
                    case 41:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isFaLun = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsFaLun(isFaLun);
                        }
                        break;
                    //   是否转化
                    case 42:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isTurn = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsTurn(isTurn);
                        }
                        break;
                    //   转化时间
                    case 43:
                        if (StringUtils.isNotBlank(cellValue)) {
                            LocalDate turnTime = checkDateTypeAndReturn(cellValue, i, j);
                            people.setTurnTime(turnTime);
                        }
                        break;
                    //   其他邪教种类
                    case 44:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setCultCategory(cellValue);
                        }
                        break;
                    //   是否吸毒
                    case 45:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isDrug = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsDrug(isDrug);
                        }
                        break;
                    //  房屋性质
                    case 46:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setHouseProper(cellValue);
                        }
                        break;
                    //   房屋间数
                    case 47:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer houseCount = checkIntTypeAndReturn(cellValue, i, j);
                            people.setHouseCount(houseCount);
                        }
                        break;
                    //   房屋面积
                    case 48:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Double houseSize = checkDoubleTypeAndReturn(cellValue, i, j);
                            people.setHouseSize(houseSize);
                        }
                        break;
                    //  产权所有人
                    case 49:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setHouseOwner(cellValue);
                        }
                        break;
                    //    使用性质
                    case 50:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setUseProper(cellValue);
                        }
                        break;
                    //   租房时间
                    case 51:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Double houseTime = checkDoubleTypeAndReturn(cellValue, i, j);
                            people.setHouseTime(houseTime);
                        }
                        break;
                    //   是否享有保障性住房
                    case 52:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isHouseProtect = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsHouseProtect(isHouseProtect);
                        }
                        break;
                    //  保障性住房类别
                    case 53:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setHouseCategory(cellValue);
                        }
                        break;
                    //    是否享有减免
                    case 54:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isGood = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsGood(isGood);
                        }
                        break;
                    //   是否就职
                    case 55:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isJob = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsJob(isJob);
                        }
                        break;
                    //    职业
                    case 56:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setJobName(cellValue);
                        }
                        break;
                    //    下岗或失业
                    case 57:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setReason(cellValue);
                        }
                        break;
                    //   再就业情况
                    case 58:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setReJob(cellValue);
                        }
                        break;
                    //   求职意向
                    case 59:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setJobIntention(cellValue);
                        }
                        break;
                    //   是否低保
                    case 60:
                        if (StringUtils.isNotBlank(cellValue)) {
                            Integer isSecurity = checkBooleanAndReturnInt(cellValue, i, j);
                            people.setIsSecurity(isSecurity);
                        }
                        break;
                    //低保类型
                    case 61:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setSecurityCategory(cellValue);
                        }
                        break;
                    //   医保类型
                    case 62:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setJoinSecurityCategory(cellValue);
                        }
                        break;
                    //    养老保险类型
                    case 63:
                        if (StringUtils.isNotBlank(cellValue)) {
                            people.setPensionCategory(cellValue);
                        }
                        break;
                    default:
                }
            }
            peopleList.add(people);
        }
        return peopleList;
    }


    public static String getCellValue(Cell cell) {
        String cellValue = "";
        if (cell == null) {
            return cellValue;
        }
        if (cell.getCellType() == CellType.NUMERIC) {
            cell.setCellType(CellType.STRING);
        }
        switch (cell.getCellType()) {
            case NUMERIC:
                cellValue = String.valueOf(cell.getNumericCellValue());
                break;
            case STRING:
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case BOOLEAN:
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case FORMULA:
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case BLANK:
                cellValue = null;
                break;
            case ERROR:
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }


    private static final List<String> PEOPLE_EXCEL_HEAD_LIST = Arrays.asList(
            "姓名", "性别", "户口性质", "与户主关系", "联系电话", "户籍码", "出生年月", "民族", "文化程度", "政治面貌",
            "组织关系所在地", "身份证号", "工作单位", "户籍所在地", "现居住地", "年收入", "是否低保", "低保类别",
            "医保类型", "养老保险类型", "是否就业", "职业", "下岗或失业", "再就业情况", "求职意向", "房屋性质",
            "房屋间数", "建筑面积", "产权所有人", "使用性质", "租房时间", "是否享有保障性住房", "保障性住房类别",
            "是否享受减免或房屋补贴", "是否流动人口", "婚姻状况", "结婚时间", "生育证号", "独生子女证领取时间",
            "独生子女证号", "流出入地址", "流出入时间", "血型", "病史", "家族史", "既往史", "药物过敏史", "有无接触辐射",
            "社区矫正", "矫正时间", "刑释解教", "帮教时间", "是否法轮功练习者", "是否转化", "转化时间", "其他邪教种类",
            "是否吸毒", "是否孤寡", "涉军类型", "入伍时间", "服役部队", "退伍时间", "安置单位");


    /**
     * 户籍码, 养殖情况, 汽车数量, 汽车类型, 互联网接入情况, 其他
     *
     * @param num
     * @return
     */
    private static final List<String> DOMICILE_HEAD_LIST = Arrays.asList(
            "户籍码", "养殖情况", "汽车数量", "汽车类型", "互联网接入情况", "其他"
    );

    public static File buildExcelByDomicileList(List<Domicile> domicileList) throws IOException {

        Workbook workbook = new XSSFWorkbook();
        workbook = new SXSSFWorkbook((XSSFWorkbook) workbook, 2000);
        Sheet sheet = workbook.createSheet("户籍信息");
        Row row = sheet.createRow(0);
        Cell cell;
        for (int i = 0; i < DOMICILE_HEAD_LIST.size(); i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(DOMICILE_HEAD_LIST.get(i));
        }
        for (int i = 0; i < domicileList.size(); i++) {
            Domicile domicile = domicileList.get(i);
            row = sheet.createRow(i + 1);
            for (int j = 0; j < DOMICILE_HEAD_LIST.size(); j++) {
                cell = row.createCell(j);
                switch (j) {
                    case 0:
                        cell.setCellValue(domicile.getDomicileCode());
                        break;
                    case 1:
                        cell.setCellValue(domicile.getAnimal());
                        break;
                    case 2:
                        cell.setCellValue(domicile.getCarCount());
                        break;
                    case 3:
                        cell.setCellValue(domicile.getCarCategory());
                        break;
                    case 4:
                        cell.setCellValue(checkIntegerAndReturnString(domicile.getInternet()));
                        break;
                    case 5:
                        cell.setCellValue(domicile.getOthers());
                        break;
                    default:
                }
            }
        }
        File file = new File(EXCEL_PATH + UUID.randomUUID().toString() + ".xlsx");
        if (!file.exists()) {
            file.createNewFile();
            try (OutputStream outputStream = new FileOutputStream(file)){
                workbook.write(outputStream);
            }
        }
        return file;
    }

    private static String checkIntegerAndReturnString(Integer num) {
        String result = "";
        if (num != null) {
            if (num == 1) {
                result = "是";
            } else {
                result = "否";
            }
        }
        return result;
    }

    private static String checkIntegerAndReturnSex(Integer sex) {
        String result = "";
        if (sex != null) {
            if (sex == 1) {
                result = "男";
            } else {
                result = "女";
            }
        }
        return result;

    }

    public static File buildExcelByPeopleList(List<People> peopleList) throws IOException {

        Workbook workbook = new XSSFWorkbook();
        workbook = new SXSSFWorkbook((XSSFWorkbook) workbook, 2000);
        Sheet sheet = workbook.createSheet("人口信息");
        Row row = sheet.createRow(0);
        Cell cell;
        for (int i = 0; i < PEOPLE_EXCEL_HEAD_LIST.size(); i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(PEOPLE_EXCEL_HEAD_LIST.get(i));
        }
        People people;
        for (int i = 0; i < peopleList.size(); i++) {
            row = sheet.createRow(i + 1);
            people = peopleList.get(i);
            for (int j = 0; j < PEOPLE_EXCEL_HEAD_LIST.size(); j++) {
                cell = row.createCell(j);
                switch (j) {
                    case 0:
                        cell.setCellValue(people.getPeopleName());
                        break;
                    case 1:
                        cell.setCellValue(checkIntegerAndReturnSex(people.getSex()));
                        break;
                    case 2:
                        cell.setCellValue(people.getResidence());
                        break;
                    case 3:
                        cell.setCellValue(people.getRelation());
                        break;
                    case 4:
                        cell.setCellValue(people.getPhone());
                        break;
                    case 5:
                        cell.setCellValue(people.getDomicileCode());
                        break;
                    case 6:
                        cell.setCellValue(DateUtil.date2Str(people.getBirthday()));
                        break;
                    case 7:
                        cell.setCellValue(people.getPeopleNational());
                        break;
                    case 8:
                        cell.setCellValue(people.getCulture());
                        break;
                    case 9:
                        cell.setCellValue(people.getPolitical());
                        break;
                    case 10:
                        cell.setCellValue(people.getOrganization());
                        break;
                    case 11:
                        cell.setCellValue(people.getCardId());
                        break;
                    case 12:
                        cell.setCellValue(people.getPeopleWork());
                        break;
                    case 13:
                        cell.setCellValue(people.getDomicileLocation());
                        break;
                    case 14:
                        cell.setCellValue(people.getNowSpace());
                        break;
                    case 15:
                        cell.setCellValue(people.getEarning());
                        break;
                    case 16:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsSecurity()));
                        break;
                    case 17:
                        cell.setCellValue(people.getSecurityCategory());
                        break;
                    case 18:
                        cell.setCellValue(people.getJoinSecurityCategory());
                        break;
                    case 19:
                        cell.setCellValue(people.getPensionCategory());
                        break;
                    case 20:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsJob()));
                        break;
                    case 21:
                        cell.setCellValue(people.getJobName());
                        break;
                    case 22:
                        cell.setCellValue(people.getReason());
                        break;
                    case 23:
                        cell.setCellValue(people.getReJob());
                        break;
                    case 24:
                        cell.setCellValue(people.getJobIntention());
                        break;
                    case 25:
                        cell.setCellValue(people.getHouseProper());
                        break;
                    case 26:
                        cell.setCellValue(people.getHouseCount());
                        break;
                    case 27:
                        cell.setCellValue(people.getHouseSize());
                        break;
                    case 28:
                        cell.setCellValue(people.getHouseOwner());
                        break;
                    case 29:
                        cell.setCellValue(people.getUseProper());
                        break;
                    case 30:
                        cell.setCellValue(people.getHouseTime());
                        break;
                    case 31:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsHouseProtect()));
                        break;
                    case 32:
                        cell.setCellValue(people.getHouseCategory());
                        break;
                    case 33:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsGood()));
                        break;
                    case 34:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsFlowPeople()));
                        break;
                    case 35:
                        cell.setCellValue(people.getMarriage());
                        break;
                    case 36:
                        cell.setCellValue(DateUtil.date2Str(people.getMarryTime()));
                        break;
                    case 37:
                        cell.setCellValue(people.getBirthCard());
                        break;
                    case 38:
                        cell.setCellValue(DateUtil.date2Str(people.getOneChildTime()));
                        break;
                    case 39:
                        cell.setCellValue(people.getOneChildCard());
                        break;
                    case 40:
                        cell.setCellValue(people.getAddress());
                        break;
                    case 41:
                        cell.setCellValue(DateUtil.date2Str(people.getHealthTime()));
                        break;
                    case 42:
                        cell.setCellValue(people.getBlood());
                        break;
                    case 43:
                        cell.setCellValue(people.getHealthHistory());
                        break;
                    case 44:
                        cell.setCellValue(people.getHomeHistory());
                        break;
                    case 45:
                        cell.setCellValue(people.getOldHistory());
                        break;
                    case 46:
                        cell.setCellValue(people.getMedicine());
                        break;
                    case 47:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsFuShe()));
                        break;
                    case 48:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsCommunityRemedy()));
                        break;
                    case 49:
                        cell.setCellValue(DateUtil.date2Str(people.getRemedyTime()));
                        break;
                    case 50:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsRelease()));
                        break;
                    case 51:
                        cell.setCellValue(DateUtil.date2Str(people.getHelpTime()));
                        break;
                    case 52:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsFaLun()));
                        break;
                    case 53:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsTurn()));
                        break;
                    case 54:
                        cell.setCellValue(DateUtil.date2Str(people.getTurnTime()));
                        break;
                    case 55:
                        cell.setCellValue(people.getCultCategory());
                        break;
                    case 56:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsDrug()));
                        break;
                    case 57:
                        cell.setCellValue(checkIntegerAndReturnString(people.getIsWidows()));
                        break;
                    case 58:
                        cell.setCellValue(people.getArmyCategory());
                        break;
                    case 59:
                        cell.setCellValue(DateUtil.date2Str(people.getInTime()));
                        break;
                    case 60:
                        cell.setCellValue(people.getForces());
                        break;
                    case 61:
                        cell.setCellValue(DateUtil.date2Str(people.getOutTime()));
                        break;
                    case 62:
                        cell.setCellValue(people.getPlace());
                        break;
                    default:
                }
            }
        }
        File file = new File(EXCEL_PATH + UUID.randomUUID().toString() + ".xlsx");
        if (!file.exists()) {
            file.createNewFile();
            try (OutputStream outputStream = new FileOutputStream(file)){
                workbook.write(outputStream);
            }
        }
        return file;
    }


}
