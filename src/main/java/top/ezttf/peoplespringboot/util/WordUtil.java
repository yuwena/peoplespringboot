package top.ezttf.peoplespringboot.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author yuwen
 * @date 2019/1/7
 */
@Slf4j
public class WordUtil {

    // private static final String TEMPLATE_FILE_PATH = "/home/developer/files/word/template/people_template.docx";
    // private static final String DOWNLOAD_FILE_PATH = "/home/developer/files/word/template/download/";

    private static final String TEMPLATE_FILE_PATH = "E:/developer/files/word/template/people_template.docx";
    private static final String DOWNLOAD_FILE_PATH = "E:/developer/files/word/template/download/";

    public static File fillFile(Map<String, String> map) throws IOException{
        InputStream inputStream = new FileInputStream(TEMPLATE_FILE_PATH);
        File file = new File(DOWNLOAD_FILE_PATH + UUID.randomUUID().toString() + ".docx");
        OutputStream outputStream = new FileOutputStream(file);
        XWPFDocument document = new XWPFDocument(inputStream);

        Iterator<XWPFTable> tablesIterator = document.getTablesIterator();
        while (tablesIterator.hasNext()) {
            XWPFTable table = tablesIterator.next();
            int numberOfRows = table.getNumberOfRows();
            for (int i = 0; i < numberOfRows; i++) {
                XWPFTableRow row = table.getRow(i);
                List<XWPFTableCell> cells = row.getTableCells();
                for (XWPFTableCell cell : cells) {
                    String text = StringUtils.strip(cell.getText(), "{}");
                    if (map.containsKey(text)) {
                        cell.removeParagraph(0);
                        cell.setText(map.get(text));
                    }
                    //for (Map.Entry<String, String> entry : map.entrySet()) {
                    //    if (StringUtils.strip(cell.getText(), "{}").equals(entry.getKey())) {
                    //        cell.removeParagraph(0);
                    //        cell.setText(entry.getValue());
                    //    }
                    //}
                }
            }
        }
        document.write(outputStream);

        inputStream.close();
        outputStream.close();
        return file;
    }



}