package top.ezttf.peoplespringboot.util;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author yuwen
 * @date 2019/1/4
 */
public class DateUtil {
    private static final String STANDARD_FORMAT = "yyyy-MM-dd";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(STANDARD_FORMAT);

    public static String dateTime2Str(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return FORMATTER.format(dateTime);
    }

    public static String dateTime2Str(LocalDateTime dateTime, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        if (dateTime == null) {
            return null;
        }
        return formatter.format(dateTime);
    }


    public static LocalDate str2Date(String dateStr) {
        if (dateStr == null || StringUtils.isBlank(dateStr)) {
            return null;
        }
        return LocalDate.parse(dateStr, FORMATTER);
    }

    public static LocalDate str2Date(String dateStr, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        if (dateStr == null || StringUtils.isBlank(dateStr)) {
            return null;
        }
        return LocalDate.parse(dateStr, formatter);
    }

    public static String date2Str(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        return FORMATTER.format(localDate);
    }
}
