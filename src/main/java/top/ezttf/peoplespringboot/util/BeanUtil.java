package top.ezttf.peoplespringboot.util;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yuwen
 * @date 2019/1/7
 */
public class BeanUtil {

    public static<T> Map<String, String> initDefaultValue(T obj) throws IllegalAccessException {
        Map<String, String> map = new HashMap<>(16);
        if (obj == null) {
            return map;
        }
        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object value = field.get(obj);
            Class<?> type = field.getType();
            if (value == null) {
                map.put(field.getName(), StringUtils.EMPTY);
            } else if (type.equals(String.class)) {
                map.put(field.getName(), value.toString());
            } else if (type.equals(Integer.class)) {
                if (StringUtils.equals("sex", fieldName)) {
                    if ((Integer) field.get(obj) == 0) {
                        map.put(fieldName, "女");
                    } else if ((Integer) field.get(obj) == 1) {
                        map.put(fieldName, "男");
                    }
                } else if (StringUtils.startsWith(fieldName, "is") || StringUtils.equals(fieldName, "internet")) {
                    if ((Integer) field.get(obj) == 0) {
                        map.put(fieldName, "否");
                    } else if ((Integer) field.get(obj) == 1) {
                        map.put(fieldName, "是");
                    }
                } else {
                    map.put(field.getName(), value.toString());
                }
            } else if (type.equals(Double.class)) {
                map.put(field.getName(), value.toString());
            } else if (type.equals(LocalDate.class)) {
                LocalDate date = (LocalDate) value;
                map.put(field.getName(), DateUtil.date2Str(date));
            }
        }
        return map;
    }
}
