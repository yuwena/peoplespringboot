package top.ezttf.peoplespringboot.config.dao;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import top.ezttf.peoplespringboot.interceptors.dao.ResultPairInterceptor;

import javax.sql.DataSource;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@Configuration
public class MyBatisConfiguration {



    @Value("${mybatis.mapper-locations}")
    private String mapperLocations;
    @Value("${mybatis.configuration.default-statement-timeout}")
    private Integer defaultStatementTimeout;
    @Value("${mybatis.configuration.cache-enabled}")
    private Boolean cacheEnable;
    @Value("${mybatis.configuration.map-underscore-to-camel-case}")
    private Boolean underCamelCase;
    @Value("${mybatis.configuration.use-generated-keys}")
    private Boolean useGeneratedKey;
    @Value("${mybatis.configuration.call-setters-on-nulls}")
    private Boolean callSetterOnNull;

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.hikari")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public ResultPairInterceptor resultPairInterceptor() {
        return new ResultPairInterceptor();
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        PathMatchingResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setDefaultStatementTimeout(defaultStatementTimeout);
        configuration.setMapUnderscoreToCamelCase(underCamelCase);
        configuration.setCallSettersOnNulls(callSetterOnNull);
        configuration.setCacheEnabled(cacheEnable);
        configuration.setUseGeneratedKeys(useGeneratedKey);

        bean.setMapperLocations(patternResolver.getResources(
                ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + mapperLocations)
        );
        bean.setDataSource(dataSource());
        bean.setTypeAliasesPackage("top.ezttf.peoplespringboot.pojo");
        bean.setPlugins(new Interceptor[]{resultPairInterceptor()});
        bean.setConfiguration(configuration);
        return bean.getObject();
    }
}
