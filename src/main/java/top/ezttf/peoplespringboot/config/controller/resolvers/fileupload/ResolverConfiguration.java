package top.ezttf.peoplespringboot.config.controller.resolvers.fileupload;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.nio.charset.StandardCharsets;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@Configuration
public class ResolverConfiguration {

    @Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setDefaultEncoding(StandardCharsets.UTF_8.name());
        multipartResolver.setResolveLazily(true);
        multipartResolver.setMaxInMemorySize(1024 * 1024 * 100);
        multipartResolver.setMaxUploadSize(1024 * 1024 * 100);
        return multipartResolver;
    }
}
