package top.ezttf.peoplespringboot.config.controller.interceptors;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.ezttf.peoplespringboot.interceptors.controller.SuperUploadPermissionInterceptor;
import top.ezttf.peoplespringboot.interceptors.controller.UploadLoginInterceptor;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@Configuration
public class InterceptorConfiguration implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration permissionInterceptor = registry.addInterceptor(new SuperUploadPermissionInterceptor());
        InterceptorRegistration loginInterceptor = registry.addInterceptor(new UploadLoginInterceptor());

        loginInterceptor.addPathPatterns("/back/**");
        loginInterceptor.excludePathPatterns("/back/login");
        loginInterceptor.order(90);

        permissionInterceptor.addPathPatterns("/back/super/**");
        permissionInterceptor.order(100);
    }

}
