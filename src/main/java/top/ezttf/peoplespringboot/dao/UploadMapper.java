package top.ezttf.peoplespringboot.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.ezttf.peoplespringboot.annotations.ResultPair;
import top.ezttf.peoplespringboot.pojo.Upload;

import java.util.List;
import java.util.Map;

/**
 * @author yuwen
 * @date 2018/12/27
 */
@Mapper
public interface UploadMapper {
    /**
     * 登录使用 根据用户名查找用户信息
     * @param username 用户名
     * @return username用户的全部信息
     */
    Upload findUploadByUsername(@Param("username") String username);


    /**
     * 查找所有区域名字信息
     * 应只有超管才可以调用此查询, 需进行上层控制
     * @param role 角色 过滤
     * @return 区域列表
     */
    List<String> findArea(@Param(value = "role") Integer role);


    /**
     * 根据区域列表查找对应上传人id列表
     * @param areaList 区域列表信息
     * @return 上传人id列表
     */
    List<Integer> findUploadIdByArea(
            @Param(value = "areaList") List<String> areaList
    );

    /**
     * 根据role 来查找管理员数量
     * @param role 查找条件  0代表查找普通管理 1 超管
     * @return
     */List<Integer> findUploadIdByRole(@Param(value = "role") Integer role);



    /**
     * 查询当前表下 存在的管理员数量
     * @return
     */
    Integer findCount();


    /**
     * 查找 uploadId 以及 username  并建立映射使uploadId作为key, username作为value
     * @return
     */
    @ResultPair
    Map<Long, String> findUploadIdAndUsername();


    /**
     * 添加管理员
     * @param upload
     * @return
     */
    @Insert("INSERT INTO tb_upload VALUES(#{uploadId}, #{username}, #{password}, #{uploadArea}, #{role}, now(), now())")
    Integer insert(Upload upload);


    /**
     * 更新 管理员信息
     * @param upload
     * @return
     */
    Integer update(Upload upload);


    /**
     * 查找 id, username, role列表
     * @return
     */
    List<Map<String, Object>> findAllUpload();


    /**
     * 通过 id 删除管理
     * @param uploadId
     * @return
     */
    @Delete("DELETE FROM tb_upload WHERE upload_id = ${uploadId}")
    Integer deleteByUploadId(@Param(value = "uploadId") Integer uploadId);

}