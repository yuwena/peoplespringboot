package top.ezttf.peoplespringboot.dao;

import org.apache.ibatis.annotations.*;
import top.ezttf.peoplespringboot.bo.Condition;
import top.ezttf.peoplespringboot.pojo.People;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author yuwen
 * @date 2018/12/27
 */
@Mapper
public interface PeopleMapper {

    /**
     * 查找某管理员下负责的人口数如果参数为null, 则返回全部人口数
     * @param uploadId 管理员id
     * @return 人口数量
     */
    Integer findPeopleCountByUploadIdOrNull(@Param("uploadId") Integer uploadId);




    /**
     * 多条件查询人口全部信息
     * @param conditionList 查询条件
     * @param uploadIdList 上传人id信息列表, 可以为null
     * @return 人口信息列表
     */
    List<People> findPeopleByArbitraryCondition(
            @Param("conditionList") List<Condition> conditionList,
            @Param("uploadIdList") List<Integer> uploadIdList
    );





    /**
     * 查找上传人的用户名(tb_upload), 以及(tb_people) 下该用户头下的用户数量
     * @param uploadIdList 上传人id 列表
     * @return
     */
    List<Map<String, Object>> findUploadNameAndPeopleCountGroupByUpload(
            @Param(value = "uploadIdList") List<Integer> uploadIdList
    );


    /**
     * 根据查询时间段 分组查找每个uploadId 下的人口数量
     * @param start 起始时间 yyyy-MM-dd HH:mm:ss
     * @param end 结束时间 yyyy-MM-dd HH:mm:ss
     * @return 人口数量
     */
    List<Map<String, Object>> findPeopleCountByCreateTimeGroupByUploadId(
            @Param(value = "start") String start,
            @Param(value = "end") String end
    );

    /**
     * 添加人口
     * @param peopleList 基本信息
     * @return
     */
    Integer batchInsert(@Param(value = "peopleList") List<People> peopleList);


    /**
     * 批量删除人
     * @param peopleIdList
     * @return
     */
    Integer batchDelete(@Param(value = "peopleIdList") List<Integer> peopleIdList);


    /**
     * 根据身份证号查找到上传人 id
     * @param cardId
     * @return
     */
    Integer findUploadIdByCardId(@Param(value = "cardId") String cardId);



    /**
     * 根据户籍码查找人口数量
     * @param domicileCOde
     * @return
     */
    @Select("SELECT COUNT(1) FROM tb_people WHERE domicile_code = #{domicileCode}")
    Integer findCountByDomicileCode(@Param(value = "domicileCode") String domicileCOde);


    /**
     * 通过人的旧户籍码更新人的户籍码
     * @param oldDomicileCode
     * @param domicileCode
     * @return
     */
    @Update("UPDATE tb_people SET domicile_code = #{domicileCode} WHERE domicile_code = #{oldDomicileCode}")
    Integer updatePeopleByDomicileCode(
            @Param(value = "oldDomicileCode") String oldDomicileCode,
            @Param(value = "domicileCode") String domicileCode
    );


    /**
     * 根据户籍码删除人口信息
     * @param domicileCode
     * @return
     */
    @Delete("DELETE FROM tb_people WHERE domicile_code = #{domicileCode}")
    Integer deletePeopleByDomicileCode(String domicileCode);


    /**
     * 根据peopleId删除人口信息
     * @param peopleId
     * @return
     */
    @Delete("DELETE FROM tb_people WHERE people_id = ${peopleId}")
    Integer deleteByPeopleId(@Param(value = "peopleId") Integer peopleId);


    /**
     * 删除某个管理元下的所有人口信息
     * @param uploadId
     * @return
     */
    @Delete("DELETE FROM tb_people WHERE upload_id = ${uploadId}")
    Integer deleteByUploadId(@Param(value = "uploadId") Integer uploadId);

    /**
     * 根据 peopleId 查找people信息
     * @param peopleId
     * @return
     */
    People findPeopleByPeopleId(@Param(value = "peopleId") Integer peopleId);


    List<People> findPeopleByUploadIdOrNull(@Param(value = "uploadId") Integer uploadId);


    @Select("SELECT people_id FROM tb_people WHERE card_id = #{cardId}")
    Integer findPeopleIdByCardId(@Param(value = "cardId") String cardId);


    @Select("SELECT upload_id FROM tb_people WHERE people_id = #{peopleId}")
    Integer findUploadIdByPeopleId(@Param(value = "peopleId") Integer peopleId);


    Set<String> findDomicileCodeDistinctByPeopleIdList(@Param(value = "peopleIdList") List<Integer> peopleIdList);
}