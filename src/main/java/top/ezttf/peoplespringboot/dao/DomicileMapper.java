package top.ezttf.peoplespringboot.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.ezttf.peoplespringboot.pojo.Domicile;

import java.util.List;
import java.util.Set;

/**
 * @author yuwen
 * @date 2018/12/27
 */
@Mapper
public interface DomicileMapper {

    /**
     * 查找uploadId下户籍总数量
     * @param uploadId 上传人id
     * @return
     */
    Integer findCountByUploadIdOrNull(@Param("uploadId") Integer uploadId);

    /**
     * 根据户籍码查找 uploadId
     * @param domicileCode
     * @return
     */
    Integer findUploadIdByDomicileCode(@Param(value = "domicileCode") String domicileCode);


    /**
     * 更新户籍信息
     * @param domicile
     * @return
     */
    Integer updateByDomicileCode(Domicile domicile);

    /**
     * 录入户籍信息
     * @param domicile
     * @return
     */
    Integer insert(Domicile domicile);


    /**
     * 根据户籍码查找户籍信息
     * @param domicileCode
     * @return
     */
    Domicile findDomicileByDomicileCode(@Param(value = "domicileCode") String domicileCode);

    /**
     * 根据户籍id 更新户籍信息 (可更新户籍码)
     * @param domicile
     * @return
     */
    Integer updateByDomicileId(Domicile domicile);


    /**
     * 根据户籍id 查找户籍码
     * @param domicileId 户籍 id
     * @return
     */
    @Select("SELECT domicile_code FROM tb_domicile WHERE domicile_id = ${domicileId}")
    String findDomicileCodeByDomicileId(@Param(value = "domicileId") Integer domicileId);

    /**
     * 根据户籍 id 删除户籍信息
     * @param domicileId
     * @return
     */
    @Delete("DELETE FROM tb_domicile WHERE domicile_id = ${domicileId}")
    Integer deleteDomicileByDomicileId(@Param(value = "domicileId") Integer domicileId);


    List<Domicile> findDomicileByUploadIdOrNull(@Param(value = "uploadId") Integer uploadId);

    @Select("SELECT domicile_code FROM tb_domicile")
    Set<String> findAllDomicileCode();

    @Delete("DELETE FROM tb_domicile WHERE domicile_code = #{domicileCode}")
    Integer deleteDomicileByDomicileCode(@Param(value = "domicileCode") String domicileCode);

    Integer batchInsert(@Param(value = "domicileList") List<Domicile> domicileList);

    Integer resetDomicileById(Integer domicileId);
}