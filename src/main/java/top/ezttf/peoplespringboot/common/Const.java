package top.ezttf.peoplespringboot.common;

/**
 * @author yuwen
 * @date 2018/12/29
 */
public class Const {

    /**
     * 当前登录用户保存session 的 key 值
     */
    public static final String CURRENT_USER = "CURRENT_USER";

    public static final String EXPORT_PEOPLE_LIST = "EXPORT_PEOPLE_LIST";


    /**
     * 管理员的相关常量接口
     */
    public interface Admin {
        /**
         * 超管
         */
        Integer SUPER_ADMIN = 1;

        /**
         * 普通管理
         */
        Integer NORMAL_ADMIN = 0;
    }
}
