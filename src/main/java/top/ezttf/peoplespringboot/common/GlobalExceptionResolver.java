package top.ezttf.peoplespringboot.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yuwen
 * @date 2019/1/6
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionResolver {

    @ExceptionHandler(value = {Exception.class})
    @ResponseBody
    public ServerResponse handlerException(HttpServletRequest request, Exception ex) {
        log.error(request.getRequestURI() + "  " + ex.toString(), ex);
        return ServerResponse.createByErrorMessage(ex.getMessage());
    }
}
