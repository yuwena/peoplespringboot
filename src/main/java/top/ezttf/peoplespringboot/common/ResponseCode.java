package top.ezttf.peoplespringboot.common;

import lombok.Getter;

/**
 * 返回信息码枚举
 * @author yuwen
 * @date 2018/12/27
 */
@Getter
public enum ResponseCode {

    /**
     * 成功
     */
    SUCCESS(0, "SUCCESS"),
    /**
     * 错误
     */
    ERROR(1, "ERROR"),
    /**
     * 未登录
     */
    NEED_LOGIN(10, "NEED_LOGIN"),

    /**
     * 权限不足
     */
    PERMISSION_DENIED(20, "PERMISSION_DENIED"),

    /**
     * 参数异常
     */
    ILLEGAL_ARGUMENT(2, "ILLEGAL_ARGUMENT");

    private final int code;
    private final String desc;


    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
