package top.ezttf.peoplespringboot.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.ezttf.peoplespringboot.bo.LineChartBo;

import java.util.List;
import java.util.Map;

/**
 * @author yuwen
 * @date 2019/1/3
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GraphVo {

    private Integer uploadCount;

    private Integer peopleCount;

    private Integer domicileCount;

    private List<Map<String, Object>> partitionList;

    private List<LineChartBo> historyUpList;
}
