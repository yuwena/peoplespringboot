package top.ezttf.peoplespringboot.service.interf;

import com.github.pagehelper.PageInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import top.ezttf.peoplespringboot.bo.Condition;
import top.ezttf.peoplespringboot.bo.PeopleDomicileBo;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.pojo.Domicile;
import top.ezttf.peoplespringboot.pojo.People;
import top.ezttf.peoplespringboot.pojo.Upload;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author yuwen
 * @date 2018/12/27
 */
public interface IUploadService {

    /**
     * 后台管理登陆
     * @param username 用户名
     * @param password 密码
     * @return 管理信息
     */
    ServerResponse<Upload> login(String username, String password);

    /**
     * 前台登陆成功后自动请求接口, 应仅允许超管调用此接口
     * @return 所有区域名字
     */
    ServerResponse<List<Map<String, String>>> initPortal();


    /**
     * 统计前端图表需要的所有数据
     * @return
     */
    ServerResponse graph();


    /**
     * 多条件检索
     * @param conditionList 条件列表
     * @param areaList 区域信息列表, 当且仅当调用此接口的用户为超管, 此字段才应该有效
     * @param upload 调用方信息
     * @param pageNo 页码
     * @param pageSize 页面大小
     * @return
     */
    ServerResponse<PageInfo<People>> findPeopleAdvance(
            List<Condition> conditionList,
            List<String> areaList,
            Upload upload,
            int pageNo,
            int pageSize
    );


    /**
     * 上传人口 excel
     * @param file 人口信息文件
     * @return
     */
    ServerResponse uploadPeopleExcel(MultipartFile file, Integer uploadId) throws Exception;


    /**
     * 上传户籍excel
     * @param file 户籍信息文件
     * @param uploadId 上传人id
     * @return
     */
    ServerResponse uploadDomicileExcel(MultipartFile file, Integer uploadId) throws Exception;


    /**
     * 添加 / 更新一个户籍以及同户下若干人口
     * @param peopleDomicileBo
     * @param uploadId
     * @return
     */
    ServerResponse add(PeopleDomicileBo peopleDomicileBo, Integer uploadId);


    /**
     * 删除人口信息
     * @param peopleId
     * @return
     */
    ServerResponse delPeople(Integer peopleId);


    /**
     * 查找户籍信息 通过户籍码
     * @param domicileCode
     * @return
     */
    ServerResponse<Domicile> getDomicileInfo(String domicileCode);

    /**
     * 根据户籍ID 更新户籍信息, 可以更新户籍码
     * 若更新户籍码则将原户籍下的人口户籍码同步更新
     * @param domicile 新户籍信息, 必须指定户籍id
     * @param oldDomicileCode 原户籍码
     * @return
     */
    ServerResponse updateDomicileInfo(Domicile domicile, String oldDomicileCode);

    /**
     * 只能根据 id 删除户籍
     * @param domicileId 户籍 id
     * @return
     */
    ServerResponse deleteDomicileInfo(Integer domicileId);

    /**
     * 更新人口信息, people中必须包含peopleId
     * @param people
     * @return
     */
    ServerResponse updatePeopleInfo(People people);

    /**
     * 添加管理员信息, 必须指定区域
     * @param upload 管理员信息
     * @return
     */
    ServerResponse addUpload(Upload upload);

    /**
     * 通过uploadId 删除管理员信息
     * @param uploadId uploadId
     * @return
     */
    ServerResponse delUpload(Integer uploadId);

    /**
     * 更新管理员信息, 必须指定uploadId
     * @param upload
     * @return
     */
    ServerResponse updateUpload(Upload upload);

    /**
     * 根据区域信息, 查找管理员详细信息
     * @return
     */
    ServerResponse<List<Map<String, Object>>> getUploadList(Integer uploadId);

    /**
     * 下载word
     * @param peopleId
     * @return
     * @throws IllegalAccessException
     * @throws IOException
     */
    ResponseEntity exportWord(Integer peopleId) throws IllegalAccessException, IOException;

    /**
     * 导出人口 excel
     * @param upload
     * @return
     */
    ResponseEntity exportPeopleExcel(Upload upload);

    /**
     * 导出户籍 excel
     * @param upload
     * @return
     */
    ResponseEntity exportDomicileExcel(Upload upload);

    /**
     * 针对查询出来的人口结果列表进行信息导出
     * @param peopleList
     * @return
     */
    ResponseEntity downLoadForPeopleListResult(List<People> peopleList);

    /**
     * 批量删除人口
     * @param idList
     * @param upload
     * @return
     */
    ServerResponse batchDeletePeople(List<Integer> idList, Upload upload);
}
