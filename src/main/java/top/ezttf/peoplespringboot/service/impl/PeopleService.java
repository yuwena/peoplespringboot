package top.ezttf.peoplespringboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.dao.PeopleMapper;
import top.ezttf.peoplespringboot.exceptions.PeopleOperationException;
import top.ezttf.peoplespringboot.pojo.People;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yuwen
 * @date 2019/1/4
 */
@Service
public class PeopleService {

    @Autowired
    private PeopleMapper peopleMapper;

    /**
     * 批量导入人口
     *
     * @param peopleList
     * @param uploadId
     * @return
     */
    public ServerResponse batchAddPeople(List<People> peopleList, Integer uploadId) {
        if (peopleList == null || peopleList.isEmpty()) {
            return ServerResponse.createByErrorMessage("人口信息不能为空");
        }
        for (People people : peopleList) {
            people.setUploadId(uploadId);
        }
        List<Integer> deletePeopleIdList = new ArrayList<>();
        for (People people : peopleList) {
            Integer upload = peopleMapper.findUploadIdByCardId(people.getCardId());
            if (upload == null) {
            } else if (!upload.equals(uploadId)) {
                throw new PeopleOperationException("无权限操作身份证号为 " + people.getCardId() + " 的人口");
            } else if (upload.equals(uploadId)) {
                Integer peopleId = peopleMapper.findPeopleIdByCardId(people.getCardId());
                deletePeopleIdList.add(peopleId);
            }
        }
        if (!deletePeopleIdList.isEmpty()) {
            try {
                peopleMapper.batchDelete(deletePeopleIdList);
            } catch (Exception e) {
                throw new PeopleOperationException("更新人口信息过程出现错误" + e.getMessage());
            }
        }
        Integer insert;
        try {
            insert = peopleMapper.batchInsert(peopleList);
        } catch (Exception e) {
            throw new PeopleOperationException("批量录入人口信息异常" + e.getMessage());
        }
        if (insert == null || insert <= 0) {
            throw new PeopleOperationException("录入失败");
        }
        return ServerResponse.createBySuccess();
    }
}
