package top.ezttf.peoplespringboot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import top.ezttf.peoplespringboot.bo.Condition;
import top.ezttf.peoplespringboot.bo.LineChartBo;
import top.ezttf.peoplespringboot.bo.PeopleDomicileBo;
import top.ezttf.peoplespringboot.common.Const;
import top.ezttf.peoplespringboot.common.ResponseCode;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.dao.DomicileMapper;
import top.ezttf.peoplespringboot.dao.PeopleMapper;
import top.ezttf.peoplespringboot.dao.UploadMapper;
import top.ezttf.peoplespringboot.exceptions.DomicileOperationException;
import top.ezttf.peoplespringboot.exceptions.PeopleOperationException;
import top.ezttf.peoplespringboot.pojo.Domicile;
import top.ezttf.peoplespringboot.pojo.People;
import top.ezttf.peoplespringboot.pojo.Upload;
import top.ezttf.peoplespringboot.service.interf.IUploadService;
import top.ezttf.peoplespringboot.util.*;
import top.ezttf.peoplespringboot.vo.GraphVo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * @author yuwen
 * @date 2018/12/27
 */
@Slf4j
@Service
public class UploadServiceImpl implements IUploadService {

    @Autowired
    private UploadMapper uploadMapper;

    @Autowired
    private PeopleMapper peopleMapper;

    @Autowired
    private DomicileMapper domicileMapper;


    @Autowired
    private PeopleService peopleService;
    @Autowired
    private DomicileService domicileService;

    /**
     * 用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @return 登录用户信息 密码擦除
     */
    @Override
    public ServerResponse<Upload> login(String username, String password) {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            return ServerResponse.createByErrorMessage("用户名或密码不能为空");
        }
        Upload upload = uploadMapper.findUploadByUsername(username);
        if (upload == null || !StringUtils.equals(upload.getPassword(), MD5Util.md5encodeutf8(password))) {
            return ServerResponse.createByErrorMessage("用户名或密码错误");
        }
        upload.setPassword(null);
        return ServerResponse.createBySuccess(upload);
    }

    /**
     * 超管初始化信息, 返回当前所有区域名字
     *
     * @return
     */
    @Override
    public ServerResponse<List<Map<String, String>>> initPortal() {
        List<String> areaList = uploadMapper.findArea(Const.Admin.NORMAL_ADMIN);
        List<Map<String, String>> list = new ArrayList<>();
        for (String s : areaList) {
            HashMap<String, String> map = new HashMap<>(3);
            map.put("value", s);
            map.put("label", s);
            list.add(map);
        }
        return ServerResponse.createBySuccess(list);
    }


    @Override
    public ServerResponse<GraphVo> graph() {
        /* 简单的三个数量统计 */
        Integer uploadCount = uploadMapper.findCount();
        Integer peopleCount = peopleMapper.findPeopleCountByUploadIdOrNull(null);
        Integer domicileCount = domicileMapper.findCountByUploadIdOrNull(null);

        List<Map<String, Object>> partitionList;
        List<LineChartBo> historyUpList = new ArrayList<>();

        // 因为可能会考虑移除超管对人的增改权限, 此处暂定以普管条件进行查询
        List<Integer> uploadIdList = uploadMapper.findUploadIdByRole(Const.Admin.NORMAL_ADMIN);

        /* 饼图 */
        partitionList = peopleMapper.findUploadNameAndPeopleCountGroupByUpload(uploadIdList);

        /* 折线图 */
        LocalDateTime early = LocalDateTime.now()
                .minus(5L, ChronoUnit.MONTHS)
                .withDayOfMonth(1)
                .withHour(0)
                .withMinute(0)
                .withSecond(0), endOfMonth;
        List<Map<String, Object>> midHistoryList;
        Map<String, List<Integer>> map = new HashMap<>(16);
        String start, end;
        for (int i = 0; i < 6; i++) {
            endOfMonth = early.plus(1L, ChronoUnit.MONTHS).minus(1L, ChronoUnit.SECONDS);
            start = DateUtil.dateTime2Str(early, "yyyy-MM-dd HH:mm:ss");
            end = DateUtil.dateTime2Str(endOfMonth, "yyyy-MM-dd HH:mm:ss");
            midHistoryList = peopleMapper.findPeopleCountByCreateTimeGroupByUploadId(start, end);
            for (Map<String, Object> objectMap : midHistoryList) {
                String username = objectMap.get("username").toString();
                Integer num = Integer.valueOf(objectMap.get("num").toString());
                if (!map.containsKey(username)) {
                    List<Integer> list = new ArrayList<>();
                    list.add(0);
                    map.put(username, list);
                } else {
                    List<Integer> list = map.get(username);
                    list.add(num);
                    map.put(username, list);
                }
            }
            early = early.plus(1L, ChronoUnit.MONTHS);
        }
        for (Map.Entry<String, List<Integer>> entry : map.entrySet()) {
            LineChartBo bo = new LineChartBo(entry.getKey(), entry.getValue());
            historyUpList.add(bo);
        }

        GraphVo graphVo = new GraphVo(uploadCount, peopleCount, domicileCount, partitionList, historyUpList);
        return ServerResponse.createBySuccess(graphVo);
    }

    /**
     * 多条件查人口 分页
     *
     * @param conditionList 条件列表
     * @param upload        上传人
     * @param pageNo        起始页
     * @param pageSize      页面大小
     * @return 分页信息
     */
    @Override
    public ServerResponse<PageInfo<People>> findPeopleAdvance(
            List<Condition> conditionList,
            List<String> areaList,
            Upload upload,
            int pageNo,
            int pageSize
    ) {
        pageNo = pageNo <= 0 ? 1 : pageNo;
        pageSize = pageSize <= 0 ? 20 : pageSize;
        List<Integer> uploadIdList;
        if (!areaList.isEmpty() && !upload.isSuperAdmin()) {
            return ServerResponse.createByErrorCodeMessage(
                    ResponseCode.PERMISSION_DENIED.getCode(),
                    "无权限制区域查询"
            );
        } else if (areaList.isEmpty() && !upload.isSuperAdmin()) {
            areaList.add(upload.getUploadArea());
        }
        areaList = areaList.isEmpty() ? null : areaList;

        /* if areaList == null 则此时可以确定操作用户必为超管,
           在此dao查询中 若areaList 为null(即当前操作用户为超管)则会自动查询全表uploadId
        */
        uploadIdList = uploadMapper.findUploadIdByArea(areaList);
        if (conditionList != null) {
            for (Condition condition : conditionList) {
                if (StringUtils.containsIgnoreCase(StringUtils.trim(condition.getRequire()), "like")) {
                    condition.setValue("%" + condition.getValue() + "%");
                }
            }
        }

        // ============================  1.25号修改查全表条件   ===============================
        // 若只有一条条件, 且为 "姓名  是  null||empty" 则查全表
        if (conditionList == null || conditionList.isEmpty()) {
            return ServerResponse.createByErrorMessage("未指定查询条件");
        }
        if (conditionList.size() == 1) {
            Condition condition = conditionList.get(0);
            if (StringUtils.equals(condition.getField().trim(), "people_name")
            && StringUtils.equals(condition.getRequire().trim(), "=")
            && StringUtils.isBlank(condition.getValue())) {
                conditionList = null;
            }
        }
        // ============================ finish ==============================================

        PageHelper.startPage(pageNo, pageSize);
        List<People> peopleList = peopleMapper.findPeopleByArbitraryCondition(conditionList, uploadIdList);
        PageInfo<People> pageInfo = new PageInfo<>(peopleList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 上传人口信息 excel, 若已存在该人口信息
     *
     * @param file 人口信息文件
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public ServerResponse uploadPeopleExcel(MultipartFile file, Integer uploadId) throws Exception {
        String originalFileName = file.getOriginalFilename();
        ExcelUtil.verifyFileName(originalFileName);
        List<People> peopleList = ExcelUtil.analysisPeopleExcel(file);
        Set<String> peopleDomicileSet = new HashSet<>();
        for (People people : peopleList) {
            peopleDomicileSet.add(people.getDomicileCode());
        }
        Set<String> domicileCodeList = domicileMapper.findAllDomicileCode();
        for (String s : peopleDomicileSet) {
            if (!domicileCodeList.contains(s)) {
                throw new RuntimeException("录入人口excel信息失败, 不存在户籍码为 " + s + " 的户籍, " +
                        "需要先录入户籍");
            }
        }
        return peopleService.batchAddPeople(peopleList, uploadId);
    }


    /**
     * 录入 excel 户籍信息, 无则添加 有则覆盖(通过户籍码判断有无)
     * @param file 户籍信息文件
     * @param uploadId 上传人id
     * @return
     * @throws IOException
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public ServerResponse uploadDomicileExcel(MultipartFile file, Integer uploadId) throws Exception {
        String originalFileName = file.getOriginalFilename();
        ExcelUtil.verifyFileName(originalFileName);
        List<Domicile> domicileList = ExcelUtil.analysisDomicileExcel(file);
        for (Domicile domicile : domicileList) {
            domicile.setUploadId(uploadId);
            String domicileCode = domicile.getDomicileCode();
            Domicile databaseDomicile = domicileMapper.findDomicileByDomicileCode(domicileCode);
            if (databaseDomicile == null) {
                domicileMapper.insert(domicile);
            } else {
                if (domicile.getUploadId().equals(uploadId) || uploadId == -1) {
                    domicileMapper.updateByDomicileCode(domicile);
                } else {
                    throw new RuntimeException("更新户籍码为 " + domicileCode + " 的户籍信息失败, 因为该户籍隶属于其他管理员");
                }
            }
        }
        return ServerResponse.createBySuccess("excel处理成功");
    }


    /**
     * 下载word
     * @param peopleId
     * @return
     * @throws IllegalAccessException
     * @throws IOException
     */
    @Override
    public ResponseEntity exportWord(Integer peopleId) throws IllegalAccessException, IOException {
        if (peopleId == null) {
            throw new RuntimeException("人口id需要传递");
        }
        People people = peopleMapper.findPeopleByPeopleId(peopleId);
        Domicile domicile = domicileMapper.findDomicileByDomicileCode(people.getDomicileCode());
        Map<String, String> map = new HashMap<>(16);
        map.putAll(BeanUtil.initDefaultValue(people));
        map.putAll(BeanUtil.initDefaultValue(domicile));
        File file = WordUtil.fillFile(map);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", "info.docx"));
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");


        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(new FileInputStream(file)));
    }


    @Override
    public ResponseEntity exportPeopleExcel(Upload upload) {
        List<People> peopleList;
        Integer uploadId = upload.isSuperAdmin() ? null : upload.getUploadId();
        peopleList = peopleMapper.findPeopleByUploadIdOrNull(uploadId);
        try {
            File file = ExcelUtil.buildExcelByPeopleList(peopleList);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", "people.xlsx"));
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(new InputStreamResource(new FileInputStream(file)));
        } catch (IOException e) {
            log.error("导出人口excel中, excel处理异常", e);
        }
        throw new RuntimeException("处理失败");
    }


    @Override
    public ResponseEntity exportDomicileExcel(Upload upload) {
        List<Domicile> domicileList;
        Integer uploadId = upload.isSuperAdmin() ? null : upload.getUploadId();
        domicileList = domicileMapper.findDomicileByUploadIdOrNull(uploadId);
        try {
            File file = ExcelUtil.buildExcelByDomicileList(domicileList);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", "domicile.xlsx"));
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(new InputStreamResource(new FileInputStream(file)));
        } catch (IOException e) {
            log.error("导出户籍excel中, excel处理异常", e);
        }
        throw new RuntimeException("处理失败");
    }


    @Override
    public ResponseEntity downLoadForPeopleListResult(List<People> peopleList) {
        try {
            File file = ExcelUtil.buildExcelByPeopleList(peopleList);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", "people.xlsx"));
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(new InputStreamResource(new FileInputStream(file)));
        } catch (IOException e) {
            log.error("导出人口excel中, excel处理异常", e);
        }
        throw new RuntimeException("处理失败");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServerResponse batchDeletePeople(List<Integer> idList, Upload upload) {
        Integer actualUploadId;
        if (!upload.isSuperAdmin()) {
            for (Integer peopleId : idList) {
                actualUploadId = peopleMapper.findUploadIdByPeopleId(peopleId);
                if (actualUploadId != null && !actualUploadId.equals(upload.getUploadId())) {
                    return ServerResponse.createByErrorMessage("越权操作");
                }
            }
        }
        Set<String> domicileCodeSet = peopleMapper.findDomicileCodeDistinctByPeopleIdList(idList);
        try {
            Integer deleteCount = peopleMapper.batchDelete(idList);
        } catch (Exception e) {
            log.error("UploadServiceImpl: batchDeletePeople -> {}", e);
            throw new PeopleOperationException("批量删除人口出现错误");
        }
        for (String domicileCode : domicileCodeSet) {
            Integer count = peopleMapper.findCountByDomicileCode(domicileCode);
            if (count != null && count == 0) {
                try {
                    domicileMapper.deleteDomicileByDomicileCode(domicileCode);
                } catch (Exception e) {
                    log.error("{}", e);
                    throw new DomicileOperationException("级联删除户籍出错");
                }
            }
        }
        return ServerResponse.createBySuccess("删除成功");
    }


    /**
     * 添加户籍以及同户人口
     *
     * @param peopleDomicileBo 户籍和多人口信息
     * @param uploadId         上传人id
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public ServerResponse add(PeopleDomicileBo peopleDomicileBo, Integer uploadId) {
        Domicile domicile = peopleDomicileBo.getDomicile();
        List<People> peopleList = peopleDomicileBo.getPeopleList();
        for (People people : peopleList) {
            people.setDomicileCode(domicile.getDomicileCode());
        }

        ServerResponse domicileResponse = domicileService.addDomicile(domicile, uploadId);
        ServerResponse peopleResponse = peopleService.batchAddPeople(peopleList, uploadId);

        if (peopleResponse.isSuccess() && domicileResponse.isSuccess()) {
            return ServerResponse.createBySuccess();
        } else if (!domicileResponse.isSuccess()) {
            return ServerResponse.createByErrorMessage("户籍信息操作失败");
        } else {
            return ServerResponse.createByErrorMessage("人口信息操作失败");
        }
    }


    /**
     * 根据人id 删除人口信息
     *
     * @param peopleId
     * @return
     */
    @Override
    public ServerResponse delPeople(Integer peopleId) {
        List<Integer> peopleIdList = new ArrayList<>();
        peopleIdList.add(peopleId);
        Integer integer = peopleMapper.batchDelete(peopleIdList);
        if (integer == null || integer <= 0) {
            return ServerResponse.createByErrorMessage("删除失败");
        }
        return ServerResponse.createBySuccess("删除成功");
    }


    /**
     * 根据户籍码查出户籍信息
     *
     * @param domicileCode
     * @return
     */
    @Override
    public ServerResponse<Domicile> getDomicileInfo(String domicileCode) {
        Domicile domicile = domicileMapper.findDomicileByDomicileCode(domicileCode);
        if (domicile == null) {
            return ServerResponse.createByErrorMessage("查询户籍不存在");
        }
        return ServerResponse.createBySuccess(domicile);
    }


    /**
     * 根据户籍id 更新户籍信息  (可更新户籍码)
     *
     * @param domicile
     * @param oldDomicileCode 原始户籍码
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public ServerResponse updateDomicileInfo(Domicile domicile, String oldDomicileCode) {
        if (domicile.getDomicileId() == null) {
            return ServerResponse.createByErrorCodeMessage(
                    ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    ResponseCode.ILLEGAL_ARGUMENT.getDesc()
            );
        }
        Integer effectCount = domicileMapper.updateByDomicileId(domicile);
        if (effectCount == null || effectCount <= 0) {
            return ServerResponse.createBySuccessMessage("更新失败");
        }
        if (!StringUtils.equals(domicile.getDomicileCode(), oldDomicileCode)) {
            // 级联将原户籍码为 oldDomicileCode 的人口户籍码进行更新
            Integer count = peopleMapper.findCountByDomicileCode(oldDomicileCode);
            if (count != null && count > 0) {
                Integer effect = peopleMapper.updatePeopleByDomicileCode(oldDomicileCode, domicile.getDomicileCode());
                if (effect != null && effect < count) {
                    return ServerResponse.createByErrorMessage("同步更新人口信息失败");
                }
            }
        }
        return ServerResponse.createBySuccess("更新成功");
    }


    /**
     * 根据户籍id 删除户籍信息
     *
     * @param domicileId 户籍 id
     * @return
     */
    @Override
    public ServerResponse deleteDomicileInfo(Integer domicileId) {
        if (domicileId == null) {
            return ServerResponse.createByErrorCodeMessage(
                    ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    ResponseCode.ILLEGAL_ARGUMENT.getDesc()
            );
        }
        String domicileCode = domicileMapper.findDomicileCodeByDomicileId(domicileId);
        // Integer deleteEffect = domicileMapper.deleteDomicileByDomicileId(domicileId);
        // to -> update
        Integer deleteEffect = domicileMapper.resetDomicileById(domicileId);
        if (deleteEffect != null && deleteEffect > 0) {
            // 级联删除户籍下的人口
            peopleMapper.deletePeopleByDomicileCode(domicileCode);
        } else {
            return ServerResponse.createByErrorMessage("删除户籍失败, 检查户籍是否存在");
        }
        return ServerResponse.createBySuccess("删除成功");
    }


    /**
     * 更新人口信息, 此处的实际逻辑为先删除再添加
     *
     * @param people
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public ServerResponse updatePeopleInfo(People people) {
        if (people == null || people.getPeopleId() == null) {
            return ServerResponse.createByErrorCodeMessage(
                    ResponseCode.ILLEGAL_ARGUMENT.getCode(),
                    ResponseCode.ILLEGAL_ARGUMENT.getDesc()
            );
        }
        peopleMapper.deleteByPeopleId(people.getPeopleId());
        List<People> peopleList = new ArrayList<>();
        peopleList.add(people);
        peopleMapper.batchInsert(peopleList);
        return ServerResponse.createBySuccess("更新成功");
    }


















    /*                                管理员管理                                          */

    /**
     * 添加管理员信息
     *
     * @param upload
     * @return
     */
    @Override
    public ServerResponse addUpload(Upload upload) {
        if (StringUtils.isBlank(upload.getUploadArea())) {
            return ServerResponse.createByErrorMessage("管理的区域信息需要填写");
        }
        upload.setUsername(upload.getUploadArea());
        upload.setPassword(MD5Util.md5encodeutf8(upload.getPassword()));
        Upload mid = uploadMapper.findUploadByUsername(upload.getUsername());
        if (mid != null) {
            return ServerResponse.createByErrorMessage("已有管理员负责此区域");
        }
        Integer effect = uploadMapper.insert(upload);
        if (effect == null || effect <= 0) {
            return ServerResponse.createByErrorMessage("添加失败");
        }
        return ServerResponse.createBySuccess();
    }

    /**
     * 通过uploadID删除管理员信息,
     *
     * @param uploadId uploadId
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public ServerResponse delUpload(Integer uploadId) {
        if (uploadId == null) {
            return ServerResponse.createByErrorMessage("参数错误");
        }
        Integer effect = uploadMapper.deleteByUploadId(uploadId);
        if (effect == null || effect <= 0) {
            return ServerResponse.createByErrorMessage("删除失败");
        }
        peopleMapper.deleteByUploadId(uploadId);
        return ServerResponse.createBySuccess("删除成功");
    }

    /**
     * 更新管理员信息, 必传uploadId
     *
     * @param upload
     * @return
     */
    @Override
    public ServerResponse updateUpload(Upload upload) {
        if (upload == null || upload.getUploadId() == null) {
            return ServerResponse.createByErrorMessage("参数错误, 需指定id");
        }
        if (StringUtils.isNotBlank(upload.getPassword())) {
            upload.setPassword(MD5Util.md5encodeutf8(upload.getPassword()));
        }
        if (StringUtils.isNotBlank(upload.getUploadArea())) {
            Upload mid = uploadMapper.findUploadByUsername(upload.getUploadArea());
            if (mid != null) {
                return ServerResponse.createByErrorMessage("已有管理员负责此区域");
            } else {
                upload.setUsername(upload.getUploadArea());
            }
        }
        Integer effect = uploadMapper.update(upload);
        if (effect == null || effect <= 0) {
            return ServerResponse.createByErrorMessage("更新失败");
        }
        return ServerResponse.createBySuccess("更新成功");
    }

    /**
     * 查找管理员信息, 必须指定所负责的区域
     *
     * @return
     */
    @Override
    public ServerResponse<List<Map<String, Object>>> getUploadList(Integer uploadId) {
        List<Map<String, Object>> uploadList = uploadMapper.findAllUpload();
        Iterator<Map<String, Object>> iterator = uploadList.iterator();
        while (iterator.hasNext()) {
            Map<String, Object> next = iterator.next();
            int id = ((Long) next.get("uploadId")).intValue();
            if (id == uploadId) {
                iterator.remove();
            }
        }
        return ServerResponse.createBySuccess(uploadList);
    }














}
