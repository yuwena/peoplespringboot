package top.ezttf.peoplespringboot.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.dao.DomicileMapper;
import top.ezttf.peoplespringboot.exceptions.DomicileOperationException;
import top.ezttf.peoplespringboot.pojo.Domicile;

/**
 * @author yuwen
 * @date 2019/1/4
 */
@Service
public class DomicileService {

    @Autowired
    private DomicileMapper domicileMapper;

    public ServerResponse addDomicile(Domicile domicile, Integer uploadId) {
        if (StringUtils.isBlank(domicile.getDomicileCode())) {
            return ServerResponse.createByErrorMessage("户籍码需要传递");
        }
        Integer upload = domicileMapper.findUploadIdByDomicileCode(domicile.getDomicileCode());
        if ((upload != null && upload.equals(uploadId)) || uploadId == -1) {
            try {
                Integer update = domicileMapper.updateByDomicileCode(domicile);
            } catch (Exception e) {
                throw new DomicileOperationException("更新户籍信息失败");
            }
        } else if (upload != null && !upload.equals(uploadId)) {
            throw new DomicileOperationException("无权限操作该户籍码");
        } else {
            domicile.setUploadId(uploadId);
            try {
                Integer insert = domicileMapper.insert(domicile);
            } catch (Exception e) {
                throw new DomicileOperationException("录入户籍信息失败" + e.getMessage());
            }
        }
        return ServerResponse.createBySuccess();
    }
}
