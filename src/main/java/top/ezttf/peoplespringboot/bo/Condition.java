package top.ezttf.peoplespringboot.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Condition {
    /**
     * 字段
     */
    private String field;

    /**
     * 条件
     */
    private String require;

    /**
     * 值
     */
    private String value;
}
