package top.ezttf.peoplespringboot.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author yuwen
 * @date 2019/1/3
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConditionAreaList {

    private List<Condition> conditionList;

    private List<String> areaList;
}
