package top.ezttf.peoplespringboot.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.ezttf.peoplespringboot.pojo.Domicile;
import top.ezttf.peoplespringboot.pojo.People;

/**
 * 一个人的详细信息, 这个人的户籍信息
 *
 * @author yuwen
 * @date 2019/1/7
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AllInfo {

    private Domicile domicile;

    private People people;
}
