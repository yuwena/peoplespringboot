package top.ezttf.peoplespringboot.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.ezttf.peoplespringboot.pojo.Domicile;
import top.ezttf.peoplespringboot.pojo.People;

import java.util.List;

/**
 * @author yuwen
 * @date 2019/1/4
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PeopleDomicileBo {

    private Domicile domicile;

    private List<People> peopleList;
}
