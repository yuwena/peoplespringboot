package top.ezttf.peoplespringboot.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author yuwen
 * @date 2019/1/4
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LineChartBo {

    /**
     * 此字段用来存放管理员管理区域名
     */
    private String name;

    /**
     * 此字段用来存放 key 为 'yyyy-MM', value 为 count的map
     */
    private List<Integer> data;

}
