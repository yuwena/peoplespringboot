package top.ezttf.peoplespringboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.ezttf.peoplespringboot.common.Const;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.pojo.Upload;
import top.ezttf.peoplespringboot.service.interf.IUploadService;

import javax.servlet.http.HttpSession;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@RestController
@RequestMapping(value = "/back/super")
public class SuperUploadController {

    @Autowired
    private IUploadService iUploadService;

    /**
     * 初始化信息
     *
     * @return
     */
    @GetMapping(value = "/init")
    public ServerResponse initPortal() {
        return iUploadService.initPortal();
    }


    /**
     * 供前端界面图表使用, 共需返回
     * uploadCount, peopleCount, domicileCount,
     * partitionList (分区统计各个区的用户数),
     * historyUpList (每个管理员6个月内上传用户数)
     *
     * @return
     */
    @GetMapping("/graph")
    public ServerResponse graph() {
        return iUploadService.graph();
    }


    /**
     * 添加一个管理, role随意
     * @param upload
     * @return
     */
    @PostMapping(value = "/add")
    public ServerResponse add(Upload upload) {
        return iUploadService.addUpload(upload);
    }

    /**
     * 删除一个 upload, 会将级联的people一并删除
     * @param uploadId
     * @return
     */
    @PostMapping(value = "/del")
    public ServerResponse del(Integer uploadId) {
        return iUploadService.delUpload(uploadId);
    }

    /**
     * 展示所有 upload, 不包括自身
     * @param session
     * @return
     */
    @GetMapping(value = "/find")
    public ServerResponse find(HttpSession session) {
        Integer uploadId = ((Upload) session.getAttribute(Const.CURRENT_USER)).getUploadId();
        return iUploadService.getUploadList(uploadId);
    }

    /**
     * 根据uploadId更新upload信息
     * @param upload
     * @return
     */
    @PostMapping(value = "/update")
    public ServerResponse update(Upload upload) {
        return iUploadService.updateUpload(upload);
    }
}
