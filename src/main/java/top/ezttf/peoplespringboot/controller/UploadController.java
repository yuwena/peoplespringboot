package top.ezttf.peoplespringboot.controller;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import top.ezttf.peoplespringboot.bo.Condition;
import top.ezttf.peoplespringboot.bo.ConditionAreaList;
import top.ezttf.peoplespringboot.bo.PeopleDomicileBo;
import top.ezttf.peoplespringboot.common.Const;
import top.ezttf.peoplespringboot.common.ServerResponse;
import top.ezttf.peoplespringboot.pojo.Domicile;
import top.ezttf.peoplespringboot.pojo.People;
import top.ezttf.peoplespringboot.pojo.Upload;
import top.ezttf.peoplespringboot.service.interf.IUploadService;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@RestController
@RequestMapping(value = "/back")
public class UploadController {

    @Autowired
    private IUploadService iUploadService;

    /**
     * 登陆
     *
     * @param username 用户名
     * @param password 密码
     * @param session  会话
     * @return 登陆信息
     */
    @PostMapping("/login")
    public ServerResponse<Upload> login(String username, String password, HttpSession session) {
        ServerResponse<Upload> serverResponse = iUploadService.login(username, password);
        Upload upload = serverResponse.getData();
        session.setAttribute(Const.CURRENT_USER, upload);
        return serverResponse;
    }


    /**
     * 退出登陆
     *
     * @param session 当前登陆会话
     * @return 成功
     */
    @GetMapping("/logout")
    public ServerResponse logout(HttpSession session) {
        session.removeAttribute(Const.CURRENT_USER);
        return ServerResponse.createBySuccess();
    }


    /**
     * 多条件查人 分页
     *
     * @param session           当前会话
     * @param conditionAreaList 中包含 多条件
     *                          areaList 区域信息列表, 若不为"" 则查看是否为超管, 当且仅当操作用户为超管, 该字段才设置有效
     * @param pageNo            页码
     * @param pageSize          页面大小
     * @return 分页信息
     */
    @PostMapping("/find")
    public ServerResponse<PageInfo<People>> findPeopleAdvance(
            @RequestBody ConditionAreaList conditionAreaList,
            HttpSession session,
            @RequestParam(required = false, defaultValue = "1") int pageNo,
            @RequestParam(required = false, defaultValue = "20") int pageSize
    ) {
        List<Condition> conditionList = conditionAreaList.getConditionList();
        List<String> areaList = conditionAreaList.getAreaList();
        Upload upload = (Upload) session.getAttribute(Const.CURRENT_USER);
        ServerResponse<PageInfo<People>> serverResponse =
                iUploadService.findPeopleAdvance(conditionList, areaList, upload, pageNo, pageSize);
        if (serverResponse.isSuccess()) {
            PageInfo<People> pageInfo = serverResponse.getData();
            List<People> peopleList = pageInfo.getList();
            session.setAttribute(Const.EXPORT_PEOPLE_LIST, peopleList);
        }
        return serverResponse;
    }


    /**
     * 添加同户人  会同时包含一个户籍  以及同户籍下若干个人
     *
     * @param peopleDomicileBo
     * @param session
     * @return
     */
    @PostMapping("/addPeople")
    public ServerResponse addPeople(
            @RequestBody PeopleDomicileBo peopleDomicileBo,
            HttpSession session
    ) {
        Integer uploadId = ((Upload) session.getAttribute(Const.CURRENT_USER)).getUploadId();
        return iUploadService.add(peopleDomicileBo, uploadId);
    }


    /**
     * 根据 peopleId 删除人口信息
     *
     * @param peopleId
     * @return
     */
    @PostMapping("/delPeople")
    public ServerResponse delPeople(Integer peopleId) {
        return iUploadService.delPeople(peopleId);
    }


    /**
     * 根据户籍码返回户籍详细信息
     *
     * @param domicileCode 户籍码
     * @return
     */
    @PostMapping("/getDomicile")
    public ServerResponse<Domicile> getDomicileInfo(String domicileCode) {
        return iUploadService.getDomicileInfo(domicileCode);
    }


    /**
     * 根据户籍码返回户籍详细信息
     *
     * @param domicile 户籍信息 必须拥有户籍id
     * @return
     */
    @PostMapping("/upDomicile")
    public ServerResponse updateDomicileInfo(Domicile domicile, String oldDomicileCode) {
        return iUploadService.updateDomicileInfo(domicile, oldDomicileCode);
    }


    /**
     * 根据户籍 id 删除户籍信息
     *
     * @param domicileId 户籍id
     * @return
     */
    @PostMapping("/delDomicile")
    public ServerResponse deleteDomicileInfo(Integer domicileId) {
        return iUploadService.deleteDomicileInfo(domicileId);
    }


    /**
     * 更新人口信息, 必须具备 peopleId
     *
     * @param people
     * @return
     */
    @PostMapping("/upPeople")
    public ServerResponse updatePeopleInfo(People people) {
        return iUploadService.updatePeopleInfo(people);
    }


    @PostMapping("/exPeople")
    public ServerResponse uploadPeopleExcel(MultipartFile file, HttpSession session) throws Exception {
        Integer uploadId = ((Upload) session.getAttribute(Const.CURRENT_USER)).getUploadId();
        return iUploadService.uploadPeopleExcel(file, uploadId);
    }


    @PostMapping("/exDomicile")
    @ResponseBody
    public ServerResponse uploadDomicileExcel(MultipartFile file, HttpSession session) throws Exception {
        Integer uploadId = ((Upload) session.getAttribute(Const.CURRENT_USER)).getUploadId();
        return iUploadService.uploadDomicileExcel(file, uploadId);
    }


    /**
     * 下载word
     *
     * @param peopleId
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     */
    @GetMapping("/down")
    public ResponseEntity downLoadWord(Integer peopleId) throws IOException, IllegalAccessException {
        return iUploadService.exportWord(peopleId);
    }

    /**
     * 导出 excel, 超管导出全部, 普通管理导出自己权限下所有 人口信息
     *
     * @param session
     * @return
     */
    @GetMapping("/exportP")
    public ResponseEntity exportPeople(HttpSession session) {
        Upload upload = (Upload) session.getAttribute(Const.CURRENT_USER);
        return iUploadService.exportPeopleExcel(upload);
    }

    @GetMapping("/exportD")
    public ResponseEntity exportDomicile(HttpSession session) {
        Upload upload = (Upload) session.getAttribute(Const.CURRENT_USER);
        return iUploadService.exportDomicileExcel(upload);
    }


    @SuppressWarnings("unchecked")
    @GetMapping("/downResult")
    public ResponseEntity downLoadPeopleListResult(HttpSession session) {
        List<People> peopleList = (List<People>) session.getAttribute(Const.EXPORT_PEOPLE_LIST);
        return iUploadService.downLoadForPeopleListResult(peopleList);
    }


    @PostMapping("/batchDeletePeople")
    public ServerResponse batchDeletePeople(@RequestBody List<Integer> idList, HttpSession session) {
        Upload upload = (Upload) session.getAttribute(Const.CURRENT_USER);
        return iUploadService.batchDeletePeople(idList, upload);
    }
}
