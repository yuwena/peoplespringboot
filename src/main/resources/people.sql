-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: people
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_domicile`
--

DROP TABLE IF EXISTS `tb_domicile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_domicile` (
  `domicile_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '户籍id',
  `domicile_code` varchar(20) NOT NULL COMMENT '户籍码',
  `upload_id` int(8) NOT NULL COMMENT '上传人id',
  `animal` varchar(100) DEFAULT '',
  `car_count` int(2) DEFAULT '0' COMMENT '汽车数量',
  `car_category` varchar(255) DEFAULT '' COMMENT '汽车种类',
  `internet` int(2) DEFAULT '1',
  `others` text,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`domicile_id`),
  UNIQUE KEY `uk_domicile_code` (`domicile_code`) USING BTREE COMMENT '户籍码唯一索引',
  KEY `tk_domicile_code_upload_id` (`domicile_code`,`upload_id`)
) ENGINE=InnoDB AUTO_INCREMENT=219991 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_people`
--

DROP TABLE IF EXISTS `tb_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_people` (
  `people_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `upload_id` int(8) NOT NULL COMMENT '上传人id',
  `card_id` varchar(20) NOT NULL COMMENT '身份证',
  `domicile_code` varchar(20) NOT NULL COMMENT '户籍码',
  `people_name` varchar(100) NOT NULL COMMENT '姓名',
  `sex` int(2) NOT NULL COMMENT '0女 1男',
  `now_space` varchar(100) NOT NULL COMMENT '现居住地',
  `phone` varchar(100) DEFAULT '' COMMENT '联系电话',
  `birthday` date NOT NULL COMMENT '出生日期',
  `relation` varchar(100) NOT NULL COMMENT '与户主关系',
  `people_national` varchar(100) NOT NULL COMMENT '民族',
  `residence` varchar(100) NOT NULL COMMENT '户口性质',
  `domicile_location` varchar(100) NOT NULL COMMENT '户籍所在地',
  `isWidows` int(2) DEFAULT '0' COMMENT '0否 1是',
  `religion` varchar(100) DEFAULT '' COMMENT '宗教信仰',
  `culture` varchar(100) NOT NULL COMMENT '文化程度',
  `political` varchar(100) NOT NULL COMMENT '政治面貌',
  `organization` varchar(100) NOT NULL COMMENT '组织关系所在地',
  `people_work` varchar(100) DEFAULT '' COMMENT '工作单位',
  `is_trouble` int(2) DEFAULT '0',
  `earning` double(10,2) DEFAULT '0.00' COMMENT '年收入',
  `army_category` varchar(100) DEFAULT '' COMMENT '涉军情况',
  `in_time` date DEFAULT NULL COMMENT '入伍时间',
  `out_time` date DEFAULT NULL COMMENT '退伍时间',
  `forces` varchar(100) DEFAULT '' COMMENT '兵役部队',
  `place` varchar(100) DEFAULT '' COMMENT '安置单位',
  `is_flow_people` int(2) DEFAULT '0' COMMENT '是否流动人口',
  `marriage` varchar(100) DEFAULT '' COMMENT '婚姻状况',
  `marry_time` date DEFAULT NULL COMMENT '结婚时间',
  `birth_card` varchar(100) DEFAULT '' COMMENT '生育证号',
  `one_child_time` date DEFAULT NULL COMMENT '独生子女证领取时间',
  `one_child_card` varchar(100) DEFAULT '' COMMENT '独生子女证号',
  `address` varchar(100) DEFAULT '' COMMENT '流出入地址',
  `health_time` date DEFAULT NULL COMMENT '流出入时间',
  `blood` varchar(20) DEFAULT '' COMMENT '血型',
  `health_history` varchar(100) DEFAULT '' COMMENT '病史',
  `home_history` varchar(100) DEFAULT '' COMMENT '家族史',
  `old_history` varchar(100) DEFAULT '' COMMENT '既往史',
  `medicine` varchar(100) DEFAULT '',
  `is_fu_she` int(2) DEFAULT '0',
  `is_community_remedy` int(2) DEFAULT '0',
  `remedy_time` date DEFAULT NULL,
  `is_release` int(2) DEFAULT '0',
  `help_time` date DEFAULT NULL,
  `is_fa_lun` int(2) DEFAULT '0',
  `is_turn` int(2) DEFAULT '0',
  `turn_time` date DEFAULT NULL,
  `cult_category` varchar(100) DEFAULT '',
  `is_drug` int(2) DEFAULT '0',
  `house_proper` varchar(100) DEFAULT '',
  `house_count` int(2) DEFAULT '0',
  `house_size` double(10,2) DEFAULT '0.00',
  `house_owner` varchar(100) DEFAULT '',
  `use_proper` varchar(100) DEFAULT '',
  `house_time` double(10,2) DEFAULT '0.00',
  `is_house_protect` int(2) DEFAULT '0',
  `house_category` varchar(100) DEFAULT '',
  `is_good` int(2) DEFAULT '0',
  `is_job` int(2) DEFAULT '1',
  `job_name` varchar(100) DEFAULT '',
  `reason` varchar(100) DEFAULT '',
  `re_job` varchar(100) DEFAULT '',
  `job_intention` varchar(100) DEFAULT '',
  `is_security` int(2) DEFAULT '0',
  `security_category` varchar(255) DEFAULT '',
  `join_security_category` varchar(100) DEFAULT '',
  `pension_category` varchar(100) DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`people_id`),
  UNIQUE KEY `uk_card_id` (`card_id`) USING BTREE,
  KEY `index_domicile_code` (`domicile_code`),
  KEY `index_phone` (`phone`),
  KEY `index_upload_id` (`upload_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_upload`
--

DROP TABLE IF EXISTS `tb_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_upload` (
  `upload_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `upload_area` varchar(100) NOT NULL COMMENT '管理区域',
  `role` int(2) NOT NULL DEFAULT '0' COMMENT '角色 0普通管理  1超管',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`upload_id`),
  UNIQUE KEY `uk_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16 16:49:54
