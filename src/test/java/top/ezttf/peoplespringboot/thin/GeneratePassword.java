package top.ezttf.peoplespringboot.thin;

import java.util.HashSet;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.ezttf.peoplespringboot.dao.DomicileMapper;
import top.ezttf.peoplespringboot.dao.UploadMapper;
import top.ezttf.peoplespringboot.pojo.Domicile;
import top.ezttf.peoplespringboot.pojo.Upload;
import top.ezttf.peoplespringboot.util.MD5Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author yuwen
 * @date 2019/1/9
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class GeneratePassword {

    @Autowired
    private UploadMapper uploadMapper;

    @Autowired
    private DomicileMapper domicileMapper;


    /**
     * Q39ry9fhe
     * Hq23fwjfg
     * Sidfjwqet1
     * W2u3i0239
     * 3U48u3042
     * 2309Ruwhu
     * We9r73y49
     * Wieour83s
     * W89ry39fh
     * 3Y7rye97f
     * Wery394fh
     * Kjzjdbsc2018
     */
    @Test
    public void insertAdmin() {
        List<Upload> list = Arrays.asList(
                new Upload(null, "商业东街", "Q39ry9fhe", "商业东街", 0, null, null),
                new Upload(null, "商业西街", "Hq23fwjfg", "商业西街", 0, null, null),
                new Upload(null, "西城郊", "Sidfjwqet1", "西城郊", 0, null, null),
                new Upload(null, "新华西街", "W2u3i0239", "新华西街", 0, null, null),
                new Upload(null, "新华东街", "3U48u3042", "新华东街", 0, null, null),
                new Upload(null, "民主东街", "2309Ruwhu", "民主东街", 0, null, null),
                new Upload(null, "民主西街", "We9r73y49", "民主西街", 0, null, null),
                new Upload(null, "气象后街", "Wieour83s", "气象后街", 0, null, null),
                new Upload(null, "西红庙东街", "W89ry39fh", "西红庙东街", 0, null, null),
                new Upload(null, "西豪丽景一", "3Y7rye97f", "西豪丽景一", 0, null, null),
                new Upload(null, "溪山美地", "Wery394fh", "溪山美地", 0, null, null),
                new Upload(null, "孔家庄街道办事处", "Kjzjdbsc2018", "孔家庄街道办事处", 1, null, null)
        );

        list.forEach(upload -> {
            upload.setPassword(MD5Util.md5encodeutf8(upload.getPassword()));
            uploadMapper.insert(upload);
        });
    }


    @Test
    public void initDomicileId() {
        int start = 110000;
        for (int i = 0; i < 11; i++) {
            List<Domicile> list = new ArrayList<>();
            for (int j = 0; j < 19999; j++) {
                start ++;
                Domicile domicile = new Domicile(null, String.valueOf(start), i + 1,
                        StringUtils.EMPTY, 0,
                        StringUtils.EMPTY, 1, StringUtils.EMPTY,
                        null, null);
                list.add(domicile);
            }
            domicileMapper.batchInsert(list);
            start ++;
            list.clear();
        }
    }

    @Test
    public void resetDomicileId() {
        int start = 110000;
        Set<String> dbCodes = domicileMapper.findAllDomicileCode();
        List<Domicile> needInsert = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            int uploadId = i + 1;
            for (int j = 0; j < 19999; j++) {
                start ++;
                String curCode = String.valueOf(start);
                if (!dbCodes.contains(curCode)) {
                    Domicile data = new Domicile(null, curCode, uploadId,
                        StringUtils.EMPTY, 0, StringUtils.EMPTY, 1,
                        StringUtils.EMPTY, null, null
                    );
                    needInsert.add(data);
                }
            }
            start ++;
        }
        if (CollectionUtils.isNotEmpty(needInsert)) {
            domicileMapper.batchInsert(needInsert);
        }
    }


}
