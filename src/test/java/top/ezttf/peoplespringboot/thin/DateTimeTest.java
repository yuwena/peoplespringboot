package top.ezttf.peoplespringboot.thin;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * @author yuwen
 * @date 2019/1/4
 */
@Slf4j
@RunWith(BlockJUnit4ClassRunner.class)
public class DateTimeTest {

    /**
     * java -Xms1g -Xmx2g -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8888,suspend=n -jar /home/developer/project/peoplespringboot-0.0.1-SNAPSHOT.jar
     */

    @Test
    public void testDateTime() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime sixMonthBefore = now.minus(1L, ChronoUnit.MONTHS)
                .withDayOfMonth(1)
                .withHour(0)
                .withMinute(0)
                .withSecond(0);
        log.error(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(sixMonthBefore));
    }
}
