package top.ezttf.peoplespringboot.dao;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author yuwen
 * @date 2018/12/29
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class UploadMapperTest {

    @Autowired
    private UploadMapper uploadMapper;

    @Autowired
    private SqlSessionFactory factory;

    @Autowired
    private PeopleMapper peopleMapper;

    @Autowired
    private DomicileMapper domicileMapper;
    @Test
    public void testAFindUploadByUsername() {
        String start = "2018-11-11 00:00:00";
        String end = "2018-11-12 00:00:00";
        List<Map<String, Object>> peopleCount = peopleMapper.findPeopleCountByCreateTimeGroupByUploadId(start, end);
        log.error(peopleCount.toString());
    }

    @Test
    public void testSet() {
        Set<String> set = domicileMapper.findAllDomicileCode();
        log.debug(set.getClass().getName());
        log.debug(set.toString());
    }

    @Test
    public void test() {
        Integer uploadId = domicileMapper.findUploadIdByDomicileCode("100025");
        log.error("{}", uploadId);
    }


}
